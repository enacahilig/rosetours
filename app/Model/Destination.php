<?php
App::uses('AppModel', 'Model');
/**
 * Destination Model
 *
 */
class Destination extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */
	public $hasMany = array(
		
		'Schedule' => array(
			'className' => 'Schedule',
		),
	);

}
;?>
