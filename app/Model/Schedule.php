<?php
App::uses('AppModel', 'Model');
/**
 * Schedule Model
 *
 */
class Schedule extends AppModel {
	
	public $belongsTo = array(
		'Driver' => array(
			'className' => 'Driver',
		),
		'Van' => array(
			'className' => 'Van',
		),
		'User' => array(
			'className' => 'User',
		),
		'Destination' => array(
			'className' => 'Destination',
		)
	);

	public $hasMany = array(
		'Reservation' => array(
			'className' => 'Reservation',
		)
	);
}
;?>
