<?php
App::uses('AppModel', 'Model');
/**
 * Reservation Model
 *
 */
class Reservation extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */
	public $belongsTo = array(
		'Customer' => array(
			'className' => 'Customer',
		),
		'Schedule' => array(
			'className' => 'Schedule',
		),
	);

}
;?>
