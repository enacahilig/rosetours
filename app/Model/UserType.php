<?php
App::uses('AppModel', 'Model');
/**
 * UserType Model
 *
 */
class UserType extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */
	public $hasMany = array(
		'User' => array(
			'className' => 'User',
		)
	);

}
;?>
