<?php
App::uses('AppModel', 'Model');
/**
 * Van Model
 *
 */
class Van extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */
	public $hasMany = array(
		
		'Schedule' => array(
			'className' => 'Schedule',
		),
	);

}
;?>
