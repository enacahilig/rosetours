<?php
App::uses('AppModel', 'Model');
/**
 * Driver Model
 *
 */
class Driver extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */
	public $hasMany = array(
		
		'Schedule' => array(
			'className' => 'Schedule',
		),
		'Feedback' => array(
			'className' => 'Feedback'
		)
	);

}
;?>
