<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class User extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */
	public $belongsTo = array(
		'UserType' => array(
			'className' => 'UserType',
		),
		
	);
	public $hasMany = array(
		
		'Schedule' => array(
			'className' => 'Schedule',
		),
	);

	/*cake way of saving secured password*/
	public function beforeSave($options = array()) {
	    if (isset($this->data[$this->alias]['password'])) {
	        $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
	    }
   		return true;
	}

	/*just checking if user is a staff*/
	public function check_if_staff($id){
		//if user is a staff
		$this->recursive=1;
		$user= $this->findById($id);
		if(strtolower($user['UserType']['type'])=="staff"){
	        return true;
		}
	}

}
;?>
