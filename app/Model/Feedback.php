<?php
App::uses('AppModel', 'Model');
/**
 * Feedback Model
 *
 */
class Feedback extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */

	var $useTable = "feedbacks";

	public $belongsTo = array(
		'Driver' => array(
			'className' => 'Driver',
		),
		'Customer' => array(
			'className' => 'Customer',
		)
	);
	

}
;?>
