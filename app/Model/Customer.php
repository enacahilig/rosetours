<?php
App::uses('AppModel', 'Model');
/**
 * User Model
 *
 */
class Customer extends AppModel {
	/**
	 * belongsTo associations
	 * 
	 * @var array
	 */
	
	public $hasMany = array(
		'Reservation' => array(
			'className' => 'Reservation',
		),
		'Feedback' => array(
			'className' => 'Feedback',
		)
	);
}
;?>
