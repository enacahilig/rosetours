<?php
/*all the functions for users adre here*/

App::uses('AppController', 'Controller');
class SchedulesController extends AppController {
	public function index(){
		
		
		$schedules = $this->Schedule->find("all");
		
		$this->set("schedules", $schedules);

		$this->loadModel('Van');
		$this->loadModel('Driver');
		$this->loadModel('Destination');
		$vans = $this->Van->find("list", array("fields"=>array("Van.id", "Van.plate_no")));
		
		
		$this->Driver->virtualFields['full_name'] =  'CONCAT(Driver.first_name, " ", Driver.last_name)';
		
		$drivers = $this->Driver->find("list", array("fields"=>array("Driver.id", "full_name")));
		$destinations = $this->Destination->find("list",array("fields"=>array("Destination.id", "Destination.place")));
		$this->set('drivers', $drivers);
		$this->set('vans', $vans);
		$this->set('destinations', $destinations);
	}
	public function add(){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");

		}

		if($this->request->is("post")){
				$schedule_exists = $this->Schedule->findByDriverIdAndVanIdAndTimeAndDate($this->request->data["Schedule"]["driver_id"], $this->request->data["Schedule"]["van_id"], date('H:i', strtotime($this->request->data["Schedule"]["time"])), date('Y-m-d', strtotime($this->request->data["Schedule"]["date"])));

				
				if($schedule_exists){
					$this->Session->setFlash(__('Duplicate schedule. Please try again.'), 'default', array('class' => 'alert alert-danger'));
				}
				else{
					$this->request->data["Schedule"]["user_id"] = $this->Auth->user("id");
					$this->request->data["Schedule"]["date"] = date('Y-m-d', strtotime($this->request->data["Schedule"]["date"]));
					$this->request->data["Schedule"]["time"] = date('H:i', strtotime($this->request->data["Schedule"]["time"]));
					$this->Schedule->create();
					$this->Schedule->save($this->request->data);
					$this->Session->setFlash(__('The schedule was successfully added.'), 'default', array('class' => 'alert alert-success'));
				}
				
							
			
		}

		$this->redirect('/schedules');
	}
	public function edit($id){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");

	}

		if($this->request->is("post") || $this->request->is("put")){
			$schedule_exists = $this->Schedule->findByDriverIdAndVanIdAndTimeAndDate($this->request->data["Schedule"]["driver_id"], $this->request->data["Schedule"]["van_id"], date('H:i', strtotime($this->request->data["Schedule"]["time"])), date('Y-m-d', strtotime($this->request->data["Schedule"]["date"])));
			if(!$schedule_exists && $this->Schedule->exists($id)){
				$this->Schedule->id = $id;
				$this->request->data["Schedule"]["time"] = date('H:i', strtotime($this->request->data["Schedule"]["time"]));
				$this->request->data["Schedule"]["date"] = date('Y-m-d', strtotime($this->request->data["Schedule"]["date"]));
				$this->Schedule->save($this->request->data);
				$this->Session->setFlash(__('The schedule was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Duplicate schedule. Please try again'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		if(isset($this->params["named"]["from_view"])){
			$this->redirect("/schedules/view/{$id}");
		}
		else{
			$this->redirect('/schedules');
		}

	}
	public function delete($id){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");

		}
		if($this->Schedule->exists($id)){
			$this->Schedule->id = $id;
			$this->Schedule->delete();
			$this->Session->setFlash(__('The schedule was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/schedules');
	}

	public function view($id){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");

		}
		$schedule = $this->Schedule->findById($id);
		$this->set("schedule", $schedule);
		$this->request->data = $schedule;

		$this->loadModel('Van');
		$this->loadModel('Driver');
		$this->loadModel('Destination');
		$vans = $this->Van->find("list", array("fields"=>array("Van.id", "Van.plate_no")));
		
		
		$this->Driver->virtualFields['full_name'] =  'CONCAT(Driver.first_name, " ", Driver.last_name)';
		
		$drivers = $this->Driver->find("list", array("fields"=>array("Driver.id", "full_name")));
		$destinations = $this->Destination->find("list",array("fields"=>array("Destination.id", "Destination.place")));
		$this->set('drivers', $drivers);
		$this->set('vans', $vans);
		$this->set('destinations', $destinations);

	}
	public function export(){
		$this->Schedule->recursive = 0;
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="rosetours_schedules.csv"');
        $fp = fopen('php://output', 'w');

        $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());

        $headers = array(
        		'Id',
        		'User',
        		'Driver Name',
        		'Van Plate Number',
        		'Time',
        		'Destination',
        		

        );
        $schedules = $this->Schedule->find("all");
		
		$this->set("schedules", $schedules);

		$this->loadModel('Van');
		$this->loadModel('Driver');
		$this->loadModel('Destination');
		$vans = $this->Van->find("list", array("fields"=>array("Van.id", "Van.plate_no")));
		$this->Driver->virtualFields['full_name'] =  'CONCAT(Driver.first_name, " ", Driver.last_name)';
		$drivers = $this->Driver->find("list", array("fields"=>array("Driver.id", "full_name")));
		$destinations = $this->Destination->find("list",array("fields"=>array("Destination.id", "Destination.place")));
		$this->set('drivers', $drivers);
		$this->set('vans', $vans);
		$this->set('destinations', $destinations);
       
 		fputcsv($fp, $headers);
 		foreach ($schedules as $schedule) {
 			$name = $schedule['User']['first_name'].' '.$schedule['User']['last_name'];
 			$driver = $schedule['Driver']['first_name'].' '.$schedule['Driver']['last_name'];
 			$time = date('h:i A', strtotime($schedule['Schedule']['time']));
 			$data = array(
					$schedule['Schedule']['id'],
					$name,
					$driver,
					$schedule['Van']['plate_no'],
					$time,
					$schedule['Destination']['place'],

			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}

	public function search(){
		$keyword = isset($this->data['Schedule']['keyword'])?$this->data['Schedule']['keyword']:'';
		$time = isset($this->data['Schedule']['time'])  && $this->data['Schedule']['time'] ? date('H:i', strtotime($this->data['Schedule']['time'])) : '';
		$this->loadModel('Van');
		$this->loadModel('Driver');
		$this->loadModel('Destination');
		$this->Driver->virtualFields['full_name'] =  'CONCAT(Driver.first_name, " ", Driver.last_name)';
		
		$conditions = "1";
		
		if($keyword){
			$conditions = " AND (Van.plate_no LIKE '%$keyword%' OR Driver.first_name LIKE '%$keyword%' OR Driver.last_name OR CONCAT(
				Driver.first_name,' ', Driver.last_name) LIKE '%$keyword%' OR Destination.place LIKE '%$keyword%')";
		}

		if($time){
			$conditions .= " AND Schedule.time LIKE '%$time%'";
		}

		$schedules = $this->Schedule->find('all', compact('conditions'));

		$vans = $this->Van->find("list", array("fields"=>array("Van.id", "Van.plate_no")));
		
		$drivers = $this->Driver->find("list", array("fields"=>array("Driver.id", "full_name")));
		$destinations = $this->Destination->find("list",array("fields"=>array("Destination.id", "Destination.place")));
		$this->set('drivers', $drivers);
		$this->set('vans', $vans);
		$this->set('destinations', $destinations);
		$this->set("schedules", $schedules);
	}
}
;?>