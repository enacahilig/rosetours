<?php
/*all the functions for customers adre here*/

App::uses('AppController', 'Controller');
class CustomersController extends AppController {
	public function index(){
		$this->check_customer();
		$customers = $this->Customer->find("all");
		$this->set("customers", $customers);
		

		
	}
	public function add(){
		
		if($this->request->is("post")){
			$this->Customer->create();
			$this->Customer->save($this->request->data);
			$this->Session->setFlash(__('The customer was successfully added.'), 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect('/customers');

	}
	public function view($id){
		
		$customer = $this->Customer->findById($id);
		$this->set("customer", $customer);
		$this->request->data = $customer;

	}
	public function edit($id){
		
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Customer->exists($id)){
				$this->Customer->id = $id;
				$this->Customer->save($this->request->data);
				$this->Session->setFlash(__('The customer was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		if(isset($this->params["named"]["from_view"])){
			$this->redirect("/customers/view/{$id}");
		}
		else{
			$this->redirect('/customers');
		}

	}

	public function delete($id){
		
		if($this->Customer->exists($id)){
			$this->Customer->id = $id;
			$this->Customer->delete();
			$this->Session->setFlash(__('The customer was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/customers');
	}
	public function upload_image($id){
		
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));

		if ($this->request->is('post')) {
			$uploaded_image = WideImage::load($_FILES["fileToUpload"]["tmp_name"]);

			$uploaded_image->resize(200,200,'fill')->saveToFile(WWW_ROOT."img".DS."customers".DS.$id.".jpg");
		}
		$this->redirect("/customers/view/{$id}/refresh:".rand(1000,9999));
	}
	public function export(){
		$this->Customer->recursive = 0;
		header('Content-Type: application/excel');
	    header('Content-Disposition: attachment; filename="rosetours_customers.csv"');  
	    $fp = fopen('php://output', 'w');

	    $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());
        
        $headers = array(
        		'Id',
        		'Name',
        		'Bad Record',
        		'Contact Number',
        		'Date Added',
        		

        );
        $customers = $this->Customer->find('all');
	      
 		fputcsv($fp, $headers);
 		foreach ($customers as $customer) {
 			$data = array(
					$customer['Customer']['id'],
					$customer['Customer']['name'],
					$customer['Customer']['bad_records_count'],
					$customer['Customer']['contact_no'],
					$customer['Customer']['date'],

			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
	public function search(){
		$keyword = isset($this->data['Customer']['keyword'])?$this->data['Customer']['keyword']:'';

		$date = isset($this->data['Customer']['date']) && $this->data['Customer']['date'] ? date('Y-m-d',strtotime($this->data['Customer']['date'])):'';

		$conditions = "1";
		
		if($keyword){
			$conditions = " AND Customer.name LIKE '%$keyword%' OR Customer.code LIKE '%$keyword%' OR Customer.bad_records_count LIKE '%$keyword%' OR Customer.contact_no LIKE '%$keyword%' ";
		}

		if($date){
			$conditions .= " AND Customer.date LIKE '%$date%'";
		}

		$customers = $this->Customer->find('all', compact('conditions'));

		$this->set('customers', $customers);
	}
	public function check_customer(){
		$customers = $this->Customer->find("all");
		$this->set("customers", $customers);
		$this->loadModel('Reservation');
		foreach ($customers as $key => $customer) {
			$reservations =$customer['Reservation'];
			$bad_record = 0;
			foreach ($reservations as $key => $reservation) {

			
				if(!$reservation['confirmed'] && (date('Y-m-d H:i:s') > $reservation['schedule_date'])){
					$bad_record++;

				}
				
				# code...
			}
			$customer['Customer']['bad_records_count'] = $bad_record;

			$this->Customer->save($customer);
		}
		


		
	}

}
;?>