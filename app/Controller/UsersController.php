<?php
/*all the functions for users adre here*/

App::uses('AppController', 'Controller');
class UsersController extends AppController {

	public function dashboard(){
		$user= $this->User->findById($this->Auth->user("id"));
		$this->loadModel('Message');
		$this->loadModel('Reservation');
		$this->loadModel('Customer');
		$this->loadModel('Van');
		$this->loadModel('Destination');
		$total_vans = $this->Van->find('count');
		$avail_vans = $this->Van->find('count', array('conditions'=> 'Van.status LIKE "%avail%"'));
		$messages = $this->Message->find('all',array('order' => array('Message.sent' => 'DESC'), "limit"=>"10"));
		$this->Reservation->recursive = 2;
		$reservations = $this->Reservation->find('all',array('order' => array('Reservation.schedule_date' => 'DESC'), "limit"=>"10"));
		$total_customers = $this->Customer->find('count');
		$total_reservations = $this->Reservation->find('count');
		$total_destinations = $this->Destination->find('count');
		$this->set('total_destinations', $total_destinations);
		$this->set('total_reservations', $total_reservations);
		$this->set('total_customers', $total_customers);	
		$this->set('avail_vans', $avail_vans);
		$this->set('messages',$messages);
		$this->set('total_vans', $total_vans);
		$this->set('reservations',$reservations);
	}

	public function login(){
		if ($this->request->is('post')) {
			$user_exist = $this->User->findByUsername($this->request->data['User']['username']);
			if ($user_exist){
				if($user_exist['User']['password']==$this->Auth->password($this->request->data['User']['password'])){
				    $this->Auth->login($user_exist['User']);
				    $this->Session->setFlash(__('You have successfully logged in.'), 'default', array('class' => 'alert alert-success'));
				    $this->redirect("/users/dashboard");
				}
				else{
					$this->Session->setFlash(__('Invalid password. Please try again.'), 'default', array('class' => 'alert alert-danger'));
  	  			}
			}
	        else{
				$this->Session->setFlash(__('Invalid username. Please try again.'), 'default', array('class' => 'alert alert-danger'));
  	  		}
  	  	}

		$this->layout = "login";
	}

	public function logout(){
		$this->redirect($this->Auth->logout());
	}
	
	public function index(){
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		$users = $this->User->find("all");
		$this->set("users", $users);

		$this->loadModel("UserType");
		$user_types = $this->UserType->find("list", array("fields"=>array("UserType.type")));
		$this->set("user_types", $user_types);
	}

	public function add(){
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		if($this->request->is("post")){
			$this->User->create();
			$this->User->save($this->request->data);
			$this->Session->setFlash(__('The user was successfully added.'), 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect('/users');
	}

	public function edit($id){
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		if($this->request->is("post") || $this->request->is("put")){
			if($this->User->exists($id)){
				$this->User->id = $id;
				$this->User->save($this->request->data);
				$this->Session->setFlash(__('The user was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		if(isset($this->params["named"]["from_view"])){
			$this->redirect("/users/view/{$id}");
		}
		else{
			$this->redirect('/users');
		}
	}

	public function settings(){
		$id = $this->Auth->user("id");
		if($this->request->is("post") || $this->request->is("put")){
			if($this->User->exists($id)){
				$this->User->id = $id;
				$this->User->save($this->request->data);
				$this->Session->setFlash(__('The user was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$this->redirect("/users/view/{$id}");	
	}

	public function delete($id){
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		if($this->User->exists($id)){
			$this->User->id = $id;
			$this->User->delete();
			$this->Session->setFlash(__('The user was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/users');
	}

	public function view($id){

		$user = $this->User->findById($id);
		$this->set("user", $user);

		if($this->User->check_if_staff($this->Auth->user("id")) && $id!==$this->Auth->user("id")){
		    $this->redirect("/users/view/".$this->Auth->user("id"));
		}

		if(!$user){
			$this->redirect("/users/view/".$this->Auth->user("id"));
		}
		$this->loadModel("UserType");
		$user_types = $this->UserType->find("list", array("fields"=>array("UserType.type")));
		$this->set("user_types", $user_types);

		$this->request->data = $user;
	}

	public function upload_image($id){
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));

		if ($this->request->is('post')) {
			$uploaded_image = WideImage::load($_FILES["fileToUpload"]["tmp_name"]);

			$uploaded_image->resize(200,200,'fill')->saveToFile(WWW_ROOT."img".DS."users".DS.$id.".jpg");
		}
		$this->redirect("/users/view/{$id}/refresh:".rand(1000,9999));
	}
	function export_users(){
		$this->User->recursive = 0;
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="rosetours_users.csv"');
        $fp = fopen('php://output', 'w');
        $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'First Name',
        		'Last Name',
        		'Contact Number',
        		'User Type',
        		'Status',
        		

        );
        $users = $this->User->find('all');
        $this->loadModel("UserType");
		$user_types = $this->UserType->find("list", array("fields"=>array("UserType.type")));
		$this->set("user_types", $user_types);
        
 		fputcsv($fp, $headers);
 		foreach ($users as $user) {
 			$status = $user["User"]["status"] ? "Active" : "Inactive";
 			$data = array(
					$user["User"]["id"],
					$user["User"]["first_name"],
					$user["User"]["last_name"],
					$user["User"]["contact_no"],
					$user["UserType"]["type"],
					$status,

			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
	public function export_reservations(){
		$this->loadModel('Reservation');
		$reservations = $this->Reservation->find('all',array('order' => array('Reservation.schedule_date' => 'DESC')));

		$this->set('reservations',$reservations);
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="rosetours_reservations.csv"');
        $fp = fopen('php://output', 'w');

        $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());

        $headers = array(
        		'Customer Name',
        		'Code',
        		'Date Reserved',
        		'Reservation Schedule',
        		'Confirmed',
        );
        
 		fputcsv($fp, $headers);
 		foreach ($reservations as $reservation) {
 			$date_reserved =  date("F, d Y h:i A", strtotime($reservation['Reservation']['date']));
 			$schedule_date = date("F, d Y", strtotime($reservation['Reservation']['schedule_date'])).' '.date("h:i A", strtotime($reservation['Schedule']['time']));
 			$confirmed = '';
 			if($reservation['Reservation']['confirmed']){
                $confirmed =  "Confirmed";
            }
            else{
                $confirmed = "Not Confirmed";
            } 
 			$data = array(
					$reservation['Customer']['name'],
					$reservation['Reservation']['code'],
					$date_reserved,
					$schedule_date,
					$confirmed,

			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
	
	public function export_messages(){
		$this->loadModel('Message');
		$messages = $this->Message->find('all',array('order' => array('Message.sent' => 'DESC')));
		$this->set('messages',$messages);
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="rosetours_messages.csv"');

        $fp = fopen('php://output', 'w');
        $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());

        $headers = array(
        		'Message',
        		'Type',
        		'Customer Contact no.',
        		'Sent',
        );
       
 		fputcsv($fp, $headers);
 		foreach ($messages as $message) {
 			$content = urldecode($message['Message']['content']);
 			$type = ucwords($message['Message']['type']);
 			$sent = date("F, d Y h:i A", strtotime($message['Message']['sent']));
 			
	 		$data = array(
					$content,
					$type,
					$message['Message']['customer_number'],
					$sent,

			);
			
			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}

	public function home(){
		$this->layout = "home";

		if($this->Auth->user()){
			$this->redirect("dashboard");
		}
	}

	public function search_user(){
		$keyword = isset($this->data['User']['keyword'])?$this->data['User']['keyword']:'';
		$this->loadModel('UserType');
		$conditions = "User.first_name LIKE '%$keyword%' OR User.last_name LIKE '%$keyword%' OR CONCAT(User.first_name,' ',User.last_name) LIKE '%$keyword%' OR User.status LIKE'%$keyword%' OR UserType.type LIKE '%$keyword%' OR User.id LIKE '%$keyword%' OR User.username LIKE '%$keyword%'";

		$users = $this->User->find('all', compact('conditions'));
		$this->set('users', $users);
	}

	public function search_reservation(){
		$this->loadModel('Reservation');
		$this->Reservation->recursive = 2;
		$keyword = isset($this->data['User']['keyword'])?$this->data['User']['keyword']:'';
		$date = isset($this->data['User']['date']) && $this->data['User']['date'] ? date('Y-m-d',strtotime($this->data['User']['date'])):'';
		$scheduled_date = isset($this->data['User']['sched_date']) && $this->data['User']['sched_date'] ? date('Y-m-d',strtotime($this->data['User']['sched_date'])):'';

		$conditions = "1";
		
		if($keyword){
			$conditions .= " AND Customer.name LIKE '%$keyword%'  ";
		}

		if($date){
			$conditions .= " AND Reservation.date LIKE '%$date%'";
		}

		if($scheduled_date){
			$conditions .= " AND Reservation.schedule_date LIKE '%$scheduled_date%'";
		}


		$reservations = $this->Reservation->find('all', compact('conditions'));

		$this->set('reservations', $reservations);
	}

	public function search_message(){
		$this->loadModel('Message');
		$keyword = isset($this->data['User']['message'])?$this->data['User']['message']:'';
		$date = isset($this->data['User']['sent_date']) &&  $this->data['User']['sent_date'] ? date('Y-m-d',strtotime($this->data['User']['sent_date'])):'';

		$conditions = "1";

		if($keyword){
			$conditions .= " AND (Message.type LIKE '%$keyword%' OR Message.content LIKE '%$keyword%' OR Message.customer_number LIKE '%$keyword%')";
		}

		if($date){
			$conditions .= " AND Message.sent LIKE '%$date%'";
		}

		$messages = $this->Message->find('all', compact('conditions'));
		
		$this->set('messages', $messages);

	}

	public function cancel_reservation($id){
		$this->loadModel("Reservation");
		if($this->Reservation->exists($id)){
			$this->Reservation->id = $id;
			$this->Reservation->saveField("confirmed", 0);
			$this->Session->setFlash(__('The Reservation was successfully cancelled.'), 'default', array('class' => 'alert alert-success'));
		}
		if(isset($this->params["named"]["page"])){
			$this->redirect("/users/reservations");
		}
		else{
			$this->redirect("/users/dashboard");
		}
		
	}

	public function messages(){
		$this->loadModel('Message');
		$messages = $this->Message->find('all',array('order' => array('Message.sent' => 'DESC')));
		$this->set('messages',$messages);
	}

	public function reservations(){
		$this->loadModel('Reservation');
		$this->Reservation->recursive = 2;
		$reservations = $this->Reservation->find('all',array('order' => array('Reservation.schedule_date' => 'DESC')));
		$this->set('reservations',$reservations);
	}
}
;?>