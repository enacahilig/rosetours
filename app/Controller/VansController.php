<?php
/*all the functions for vans add here*/

App::uses('AppController', 'Controller');
class VansController extends AppController {
	public function index(){
		
		$vans = $this->Van->find("all");
		$this->set("vans", $vans);

		
	}
	public function add(){
		
		if($this->request->is("post")){
			$this->Van->create();
			$this->Van->save($this->request->data);
			$this->Session->setFlash(__('The van was successfully added.'), 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect('/vans');

	}
	public function view($id){
		
		$van = $this->Van->findById($id);
		$this->set("van", $van);
		$this->request->data = $van;

	}
	public function edit($id){
		
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Van->exists($id)){
				$this->Van->id = $id;
				$this->Van->save($this->request->data);
				$this->Session->setFlash(__('The van was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		if(isset($this->params["named"]["from_view"])){
			$this->redirect("/vans/view/{$id}");
		}
		else{
			$this->redirect('/vans');
		}

	}

	public function delete($id){
		
		if($this->Van->exists($id)){
			$this->Van->id = $id;
			$this->Van->delete();
			$this->Session->setFlash(__('The van was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/vans');
	}
	public function upload_image($id){
		
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));

		if ($this->request->is('post')) {
			$uploaded_image = WideImage::load($_FILES["fileToUpload"]["tmp_name"]);

			$uploaded_image->resize(200,200,'fill')->saveToFile(WWW_ROOT."img".DS."vans".DS.$id.".jpg");
		}
		$this->redirect("/vans/view/{$id}/refresh:".rand(1000,9999));
	}
	
	public function export(){
		$this->Van->recursive = 0;
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="rosetours_vans.csv"');
        $fp = fopen('php://output', 'w');
        $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'Plate Number',
        		'Status',
        		'Capacity',
        	
        );
        $vans = $this->Van->find('all');
 		fputcsv($fp, $headers);
 		foreach ($vans as $van) {
 			$data = array(
					$van['Van']['id'],
					$van['Van']['plate_no'],
					$van['Van']['status'],
					$van['Van']['capacity'],
					

			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
	public function search(){

		$keyword = isset($this->data['Van']['keyword'])?$this->data['Van']['keyword']:'';
		$conditions = "Van.plate_no LIKE '%$keyword%' OR Van.status LIKE '%$keyword%' OR Van.capacity LIKE '%$keyword%'";
		$vans = $this->Van->find('all', compact('conditions'));
		$this->set("vans", $vans);
		
		
	}


}
;?>