<?php
/*all the functions for chikka add here*/

/*
	go to https://api.chikka.com/
	login as:
	ma.jayannebautista@gmail.com
	pass: rosetours
*/
App::uses('AppController', 'Controller');
class ChikkaController extends AppController {
	
	public function receiver(){
		$this->loadModel("Customer");
		$this->loadModel("Destination");
		$this->loadModel("Schedule");
		$this->loadModel("Reservation");
		$this->loadModel("Feedback");

		//test
		/*$_POST["mobile_number"] = '639178643319';
		$_POST["message"] = "Test Message";
		$_POST["shortcode"] =  "29290809571";
		$_POST["request_id"] = "asdkja2lsdqwe";*/

        $message = strtolower($_POST["message"]);
        $mobile_number = $_POST["mobile_number"];
       	$shortcode = $_POST["shortcode"];
        $timestamp = $_POST["timestamp"];
        $request_id = $_POST["request_id"];


        $this->loadModel("Message");
	    $this->Message->create();
        $this->Message->save(
        	array(
        		"message_id" => $request_id,
        		"customer_number" =>  $mobile_number,
        		"type" => "incoming",
        		"content" => $message,
        		"delivered" => "1",
        		"sent" => date("Y-m-d H:i:s"),
        		"request_id"=>  $request_id 
        	)
        );

		/*$message = "RESERVE 2015-08-28 01:00AM ILOILO";
		$_POST["mobile_number"] = 1234567;*/

        //add code here if user texted our site, here are the cases:

        //if user registered REGISTER NAME:ENA CAHILIG
        try{

	       	if (preg_match('/register/', $message)){
	       		$message_str = explode("name:", $message);
	       		$name = ucwords($message_str[1]);

	       		// If Customer exists
	       		if($this->Customer->findByContactNo( $_POST["mobile_number"] )){
	       			//ALREADY REGISTERED MESSAGE
	       			
	       			//list all destinations
	       			$destinations = $this->Destination->find("list", array("fields"=>array("place")));
	        		$destinations = strtoupper(implode(",", $destinations)); //Iloilo, Antique

	        		$today = date("Y-m-d");
	       			$response_message1 = "This number is already registered in Rose Tours. For reservations text RESERVE DATE TIME DESTINATION ex. RESERVE {$today} 08:30AM ILOILO.";

	       			//$response_message2 = " Here are the availble destinations {$destinations}.";

	       			//just for debugging
	       			//echo "MESSAGE 1: ".$response_message1;
	       			//echo "<br>LENGTH: ".strlen($response_message1);
	       			////echo "<br><br>MESSAGE 2: ".$response_message2;
	       			////echo "<br>LENGTH: ".strlen($response_message2);

	       			//send the messages, 2 messages to avoid some text missing

			      	$success = $this->send_text($_POST["mobile_number"],$response_message1,  $request_id);
			      	if(!$success){
			      		$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
			      	}
	      
	       		}
	       		else{
	       			// Save Customer

	       			$this->Customer->create();
			   		$this->Customer->save(
			   			array(
			   				"name"=>$name,
			   				"contact_no"=> $_POST["mobile_number"],
			   				"date"=>date("Y-m-d H:i:s")
			   			)
			   		);

	        		$destinations = $this->Destination->find("list", array("fields"=>array("place")));
	        		$destinations = strtoupper(implode(",", $destinations));;

	        		$today = date("Y-m-d");
	       			$response_message1 = "Thank you for registering in Rose Tours. For reservations text RESERVE DATE TIME DESTINATION ex. RESERVE {$today} 08:30AM ILOILO.";

	       			$response_message2 = " Here are the availble destinations {$destinations}.";

	       			//echo "MESSAGE 1: ".$response_message1;
	       			//echo "<br>LENGTH: ".strlen($response_message1);
	       			//echo "<br><br>MESSAGE 2: ".$response_message2;
	       			//echo "<br>LENGTH: ".strlen($response_message2);

			      	$success = $this->send_text($_POST["mobile_number"],$response_message1,  $request_id);
			      	
			      	if(!$success){
			      		$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
			      	}

			      	$success = $this->send_text($_POST["mobile_number"],$response_message2);
			      	if(!$success){
			      		$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
			      	}

	       		}
	       	}

	        //if user reserved for a date RESERVE yyyy-mm-dd h:i:A ILOILO then reply if sched is avail (ask user to confirm), or sched is not avail, reply with (closest sched)
	       	else if (preg_match('/reserve/', $message)){
	       		//split the texts
	       		$message_str = explode(" ", $message);
	       		$date = $message_str[1];
	       		$time = $message_str[2];
	       		$destination = $message_str[3];
	       		
	       		//find if number exists in database
	       		if($customer = $this->Customer->findByContactNo( $_POST["mobile_number"] )){
	       			//check first if customer's bad record is less than 5

	       			if($customer['Customer']['bad_records_count']<5){
		       			$today = date("Y-m-d");
		       			if(new DateTime($date) >= new DateTime($today)){
		       				//get destination id
			       			$destination_data = $this->Destination->findByPlace(ucwords($destination));

			       			if($destination_data){
				              	//if registered then check van schedules with the given destination and time
				       			$schedules = $this->Schedule->findAllByTimeAndDestinationIdAndDate(date('H:i', strtotime($time)), $destination_data["Destination"]["id"], date('Y-m-d', strtotime($date)));

				       			//check seats for each sched
				       			$has_seats = false;
				       			foreach($schedules as $schedule){
				       				//vail seats = capacity of van - the customers that set a reservation for that day
				       				$seats = $schedule["Van"]["capacity"] - count($schedule["Reservation"]);
				       				if($seats>0){
				       					$has_seats = true;
				       					//if seat is already found then send a message!

				       					//generate 6 digit random string for code
										$length = 6;
										$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
									    $charactersLength = strlen($characters);
									    $randomString = '';
									    for ($i = 0; $i < $length; $i++) {
									        $randomString .= $characters[rand(0, $charactersLength - 1)];
									    }

									    $reservation_code = $randomString;

						        		//create reservation
						        		$this->Reservation->create();
						        		$this->Reservation->save(
						        			array(
						        				"schedule_id"=>$schedule["Schedule"]["id"],
						        				"customer_id"=>$customer["Customer"]["id"],
						        				"date"=>date("Y-m-d H:i:s"),
						        				"code"=>$reservation_code,
						        				"confirmed"=>0,
						        				"schedule_date"=>date("Y-m-d", strtotime($date))
						        			)
						        		);	

						       			$response_message = "We have a vacant seat for this schedule {$date} {$time} {$destination}. Here is your reservation code. Please confirm by texting CONFIRM {$reservation_code}";

						       			//for debugging
						       			//echo "MESSAGE: ".$response_message;
						       			//echo "<br>LENGTH: ".strlen($response_message);

								      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
								      	if(!$success){
								      		$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
								      	}
								      	break;
				       				}
				       			}

				       			//ifno seats availaboe
				       			if(!$has_seats){
				       				$schedules = $this->Schedule->findAllByDestinationId($destination_data["Destination"]["id"]);
				       				$available = [];
				       				foreach ($schedules as $key => $schedule) {
				       					$seats = $schedule["Van"]["capacity"] - count($schedule["Reservation"]);
				       					if($seats>0){
				       						$available[]=date("h:iA", strtotime($schedule["Schedule"]["time"]));
				       					}
				       				}


				       				$available_str = implode(", ", array_unique($available));
				       				//query all other schedules

				       				if($available){
				       					$response_message = "We have no vacant seat for this schedule {$date} {$time} {$destination}. Here are the available schedule on that day {$available_str}. Text us again for another reservation.";
				       				}
				       				else{
				       					$response_message = "We have no vacant seat for this schedule {$date} {$time} {$destination}.";
				       				}
				       				

					       			//echo "MESSAGE: ".$response_message;
					       			//echo "<br>LENGTH: ".strlen($response_message);

							      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
							      	if(!$success){
								      	$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
								    }
				       			}
				       			
				       		}
				       		else{
				       			//no destination found. If destination is invalid
				       			$destinations = $this->Destination->find("list", array("fields"=>array("place")));
				        		$destinations = strtoupper(implode(",", $destinations));;

				        		$today = date("Y-m-d");
				       			$response_message = "{$destination} is invalid. Here are the availble destinations {$destinations}.";

				       			//for debugging
				       			//echo "MESSAGE: ".$response_message;
				       			//echo "<br>LENGTH: ".strlen($response_message);

						      	$success =  $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
						      	if(!$success){
								    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
								}
				       		}
		       			}
		       			//if date is invalid
		       			else{
		       				$response_message = "{$date} is invalid. Please try again.";

			       			//echo "MESSAGE: ".$response_message;
			       			//echo "<br>LENGTH: ".strlen($response_message);

					      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
					      	if(!$success){
								$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
							}
		       			}
		       		}
	       			else{
	       				//do not entertain, do not even send a message since the user has bad records of >=5
	       			}
	       		}
	       		else{
			   		//reply that customer is not yet registered
			   		//$this->send_text($_POST["mobile_number"], "You are not registered in Rose Tours. To register, text REGISTER NAME:{YOUR NAME HERE}. For example, REGISTER NAME:JJ Lopez");
			   		$response_message = "You are not registered in Rose Tours. To register, text REGISTER NAME:{YOUR NAME HERE}. For example, REGISTER NAME:JJ Lopez.";

	       			//echo "MESSAGE: ".$response_message;
	       			//echo "<br>LENGTH: ".strlen($response_message);

			      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
			      	if(!$success){
					    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
					}
	       		}
	       	}

	        //if user confirmed a sched, save to table reservations, update confirmed flag
	       	else if (preg_match('/confirm/', $message)){
	       		$message_str = explode(" ", $message);
	       		$code = $message_str[1];
	    
	       		if($this->Customer->findByContactNo( $_POST["mobile_number"] )){
	       			//fin reservation and mark as 1
	       			$this->Reservation->recursive = 2;
	       			$reservation = $this->Reservation->findByCode($code);  

	       			if($reservation){
	       				$this->Reservation->id = $reservation["Reservation"]["id"];
	       				$this->Reservation->saveField("confirmed",1);

	       				//echo "You have reserved your trip with this {$schedule} and destination {$destination}";
	       				//$this->send_text($_POST["mobile_number"], "You have reserved your trip with this {$schedule} and destination {$destination}");

	       				$time = date("h:iA", strtotime($reservation["Schedule"]["time"]));
	       				$date = date("Y-m-d", strtotime($reservation["Reservation"]["schedule_date"]));
	       				$response_message = "You have reserved your trip with Rose Tours at {$date} {$time} to {$reservation["Schedule"]["Destination"]["place"]}. Thank you!";

		       			//echo "MESSAGE: ".$response_message;
		       			//echo "<br>LENGTH: ".strlen($response_message);

				      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
				      	if(!$success){
						    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
						}
	       			}	
	       			else{
	       				$response_message = "Invalid reservation code. Please try again. Thank you!";

		       			//echo "MESSAGE: ".$response_message;
		       			//echo "<br>LENGTH: ".strlen($response_message);

				      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
				      	if(!$success){
						    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
						}
	       			}		

	       		}
	       		else{
			   		//reply that customer is not yet registered
			   		//$this->send_text($_POST["mobile_number"], "You are not registered in Rose Tours. To register, text REGISTER NAME:{YOUR NAME HERE}. For example, REGISTER NAME:JJ Lopez");
			   		$response_message = "You are not registered in Rose Tours. To register, text REGISTER NAME:{YOUR NAME HERE}. For example, REGISTER NAME:JJ Lopez";
			   		//echo "MESSAGE: ".$response_message;
	       			//echo "<br>LENGTH: ".strlen($response_message);

			      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
			      	if(!$success){
					    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
					}
	       		}
	       	}

	       	//if user sent a feedback FEEDBACK {code} COMMENT:{message}
	       	else if (preg_match('/feedback/', $message)){
	       		$message_str = explode("comment:", $message);
	       		$code_str = explode(" ", $message_str[0]);
	       		$code = $code_str[1];
	       		$comment = $message_str[1];
	    
	       		if($customer = $this->Customer->findByContactNo( $_POST["mobile_number"] )){
	       			//find reservation 
	       			$this->Reservation->recursive = 2;
	       			$reservation = $this->Reservation->findByCode($code);  

	       			if($reservation){
	       				//save driver_id, customer_id and comment of customer
	       				$this->Feedback->create();
	       				$this->Feedback->save(array(
	       					"customer_id" => $customer["Customer"]["id"],
	       					"driver_id" => $reservation["Schedule"]["driver_id"],
	       					"comment" => $comment
	       				));

	       				//echo "You have reserved your trip with this {$schedule} and destination {$destination}";
	       				//$this->send_text($_POST["mobile_number"], "You have reserved your trip with this {$schedule} and destination {$destination}");

	       				$response_message = "Rose Tours will review your feedback right away. Thank you.";

		       			//echo "MESSAGE: ".$response_message;
		       			//echo "<br>LENGTH: ".strlen($response_message);

				      	$success = $this->send_text($_POST["mobile_number"], $response_message,  $request_id);
				      	if(!$success){
						    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
						}
	       			}	
	       			else{
	       				$response_message = "Invalid reservation code. Please try again. Thank you!";

		       			//echo "MESSAGE: ".$response_message;
		       			//echo "<br>LENGTH: ".strlen($response_message);

				      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
				      	if(!$success){
						    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
						}
	       			}		

	       		}
	       		else{
			   		//reply that customer is not yet registered
			   		//$this->send_text($_POST["mobile_number"], "You are not registered in Rose Tours. To register, text REGISTER NAME:{YOUR NAME HERE}. For example, REGISTER NAME:JJ Lopez");
			   		$response_message = "You are not registered in Rose Tours. To register, text REGISTER NAME:{YOUR NAME HERE}. For example, REGISTER NAME:JJ Lopez";
			   		//echo "MESSAGE: ".$response_message;
	       			//echo "<br>LENGTH: ".strlen($response_message);

			      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
			      	if(!$success){
					    $this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
					}
	       		}
	       	}

	       	else{
	       		  //if user reply nonsense, dont reply
	       		$response_message = "Invalid keywords. Please check our website for more information.";
		   		//echo "MESSAGE: ".$response_message;
	   			//echo "<br>LENGTH: ".strlen($response_message);

		      	$success = $this->send_text($_POST["mobile_number"],$response_message,  $request_id);
		      	if(!$success){
					$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
				}
	       	}

	       	echo "Accepted";
	   	}
	   	catch (Exception $e){
	   		$success = $this->send_text($_POST["mobile_number"], "Something went wrong. Please try again.",  $request_id);
	   		if(!$success){
				$this->send_text($_POST["mobile_number"], "Something went wrong. PLease try again.");
			}

			echo "Error";
	   	}

       
        exit;

	}

	public function notification_receiver(){
		//check if message is delivered
        $this->loadModel("Message");
		$message = $this->Message->findByMessageId( $_POST["message_id"]  );

		if($message){
			$this->Message->id = $message["Message"]["id"];
	        $this->Message->save(
	        	array(
	        		"delivered" =>$_POST["status"],
	        		"modified" => date("Y-m-d H:i:s")
	        	)
			);

			echo "Accepted";
		}	
		else{
			echo "Error";
		}

		exit;	
	}

	public function send_text($mobile_number, $message, $request_id=0){

		//check App/Config/boostrap.php, the values of the client id, short code, secret key are there. These are the credentials provided by Chikka API
		$client_id = Configure::read('client_id');
		$short_code = Configure::read('short_code');	
		$secret_key = Configure::read('secret_key');

		//generate 10 digit random string for message id
		$length = 10;
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }

	    $message_id = $randomString;
		$message = urlencode($message);
		
		if($request_id){
			$arr_post_body = array(
		        "message_type" => "REPLY",
		        "mobile_number" => $mobile_number,
		        "shortcode" => $short_code,
		        "message_id" => $message_id,
		        "message" => $message ,
		        "client_id" => $client_id,
		        "secret_key" => $secret_key,
		        "request_id" => $request_id,
		        "request_cost"=>"FREE"
	    	);
		}
		else{
			$arr_post_body = array(
		        "message_type" => "SEND",
		        "mobile_number" => $mobile_number,
		        "shortcode" => $short_code,
		        "message_id" => $message_id,
		        "message" => $message ,
		        "client_id" => $client_id,
		        "secret_key" => $secret_key
	        );
		}
		

		$this->loadModel("Message");
	    $this->Message->create();
        $this->Message->save(
        	array(
        		"message_id" => $message_id,
        		"customer_number" => $mobile_number,
        		"type" => "outgoing",
        		"content" => $message,
        		"delivered" => "0",
        		"sent" => date("Y-m-d H:i:s"),
        		"request_id" => $request_id
        	)
        );

	    $query_string = "";
	    foreach($arr_post_body as $key => $frow)
	    {
	        $query_string .= '&'.$key.'='.$frow;
	    }

	    $URL = "https://post.chikka.com/smsapi/request";
	    
	    
	   	try {
	   		$curl_handler = curl_init();
		    curl_setopt($curl_handler, CURLOPT_URL, $URL);
		    curl_setopt($curl_handler, CURLOPT_POST, count($arr_post_body));
		    curl_setopt($curl_handler, CURLOPT_POSTFIELDS, $query_string);
		    curl_setopt($curl_handler, CURLOPT_RETURNTRANSFER, TRUE);
		    $response = curl_exec($curl_handler);
		    curl_close($curl_handler);
	        return true;
	   	} catch (Exception $e) {
	   		return false;
	   	}
	}
}
;?>