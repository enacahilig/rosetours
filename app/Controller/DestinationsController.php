<?php
/*all the functions for destinations add here*/

App::uses('AppController', 'Controller');
class DestinationsController extends AppController {
	public function index(){
		$destinations = $this->Destination->find("all");
		$this->set("destinations", $destinations);
	}
	public function add(){
		if($this->request->is("post")){
			$this->Destination->create();
			$this->request->data["Destination"]["place"] = ucwords($this->request->data["Destination"]["place"]);
			$this->Destination->save($this->request->data);
			$this->Session->setFlash(__('The destination was successfully added.'), 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect('/destinations');

	}
	public function view($id){
		$destination = $this->Destination->findById($id);
		$this->set("destination", $destination);
		$this->request->data = $destination;

	}
	public function edit($id){
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Destination->exists($id)){
				$this->Destination->id = $id;
				$this->request->data["Destination"]["place"] = ucwords($this->request->data["Destination"]["place"]);
				$this->Destination->save($this->request->data);
				$this->Session->setFlash(__('The destination was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		if(isset($this->params["named"]["from_view"])){
			$this->redirect("/destinations/view/{$id}");
		}
		else{
			$this->redirect('/destinations');
		}
	}
	public function delete($id){
		if($this->Destination->exists($id)){
			$this->Destination->id = $id;
			$this->Destination->delete();
			$this->Session->setFlash(__('The destination was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/destinations');

	}
	public function export(){
		$this->Destination->recursive = 0;
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="rosetours_destinations.csv"');
        $fp = fopen('php://output', 'w');
        $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'Place',
        		
        );

        $destinations = $this->Destination->find('all');
        
 		fputcsv($fp, $headers);
 		foreach ($destinations as $destination) {
 			$data = array(
					$destination['Destination']['id'],
					$destination['Destination']['place'],
					

			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
	public function search(){
		$keyword = isset($this->data['Destination']['keyword'])?$this->data['Destination']['keyword']:'';
		$conditions = "Destination.place LIKE '%$keyword%'";
		$destinations = $this->Destination->find('all', compact('conditions'));
		$this->set("destinations", $destinations);
		
	}

}

;?>