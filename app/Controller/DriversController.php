<?php
/*all the functions for drivers add here*/

App::uses('AppController', 'Controller');
class DriversController extends AppController {
	public function index(){
		
		$drivers = $this->Driver->find("all");
		$this->set("drivers", $drivers);

		
	}
	public function add(){
		
		if($this->request->is("post")){
			$this->Driver->create();
			$this->Driver->save($this->request->data);
			$this->Session->setFlash(__('The driver was successfully added.'), 'default', array('class' => 'alert alert-success'));
		}
		$this->redirect('/drivers');

	}
	public function view($id){
		$this->Driver->recursive = 2;
		$driver = $this->Driver->findById($id);
		$this->set("driver", $driver);
		$this->request->data = $driver;

		$this->loadModel('Reward');
		$conditions = "Reward.driver_id=$id";
		
		$rewards = $this->Reward->find('all', compact('conditions'));
		
		$this->set("rewards", $rewards);
	}
	public function edit($id){
		
		if($this->request->is("post") || $this->request->is("put")){
			if($this->Driver->exists($id)){
				$this->Driver->id = $id;
				$this->Driver->save($this->request->data);
				$this->Session->setFlash(__('The driver was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		if(isset($this->params["named"]["from_view"])){
			$this->redirect("/drivers/view/{$id}");
		}
		else{
			$this->redirect('/drivers');
		}

	}

	public function delete($id){
		
		if($this->Driver->exists($id)){
			$this->Driver->id = $id;
			$this->Driver->delete();
			$this->Session->setFlash(__('The driver was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/drivers');
	}
	public function upload_image($id){
		
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));
		App::import('Vendor', 'WideImage',array('file' => 'WideImage'.DS.'WideImage.php'));

		if ($this->request->is('post')) {
			$uploaded_image = WideImage::load($_FILES["fileToUpload"]["tmp_name"]);

			$uploaded_image->resize(200,200,'fill')->saveToFile(WWW_ROOT."img".DS."drivers".DS.$id.".jpg");
		}
		$this->redirect("/drivers/view/{$id}/refresh:".rand(1000,9999));
	}
	public function add_rewards($id){
		$this->loadModel('Reward');
		if($this->Driver->exists($id)){
			if($this->request->is("post")){
				$this->Reward->create();
				$this->Reward->save($this->request->data);
				$this->Session->setFlash(__('The reward was successfully added.'), 'default', array('class' => 'alert alert-success'));
			}

		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		if(isset($this->params["named"]["from_view"])){
			$this->redirect("/drivers/view/{$id}");
		}
		else{
			$this->redirect('/drivers');
		}
	}
	public function export(){
		$this->Driver->recursive = 0;
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="rosetours_drivers.csv"'); 
        $fp = fopen('php://output', 'w');
        $rosetours = array(
        		'',
        		'',
        		'Company: Rose Tours',
        );

        fputcsv($fp, $rosetours);

        $address = array(
        		'',
        		'',
        		'Address: Antique',
        );

        fputcsv($fp, $address);

        $number = array(
        		'',
        		'',
        		'Contact us @ 29290809571',
        );

        fputcsv($fp, $number);

 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'First Name',
        		'Last Name',
        		'License Number',
        		
        );
        $drivers = $this->Driver->find('all');
       
 		fputcsv($fp, $headers);
 		foreach ($drivers as $driver) {
 			$data = array(
					$driver['Driver']['id'],
					$driver['Driver']['first_name'],
					$driver['Driver']['last_name'],
					$driver['Driver']['license_no'],
					

			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
	public function search(){
		$keyword = isset($this->data['Driver']['keyword'])?$this->data['Driver']['keyword']:'';
		$conditions = "Driver.first_name LIKE '%$keyword%' OR Driver.last_name  LIKE '%$keyword%' OR CONCAT(Driver.first_name,' ', Driver.last_name)LIKE '%$keyword%' OR Driver.license_no LIKE '%$keyword%'";
		$drivers = $this->Driver->find('all', compact('conditions'));
		$this->set("drivers", $drivers);
		
		
	}


}
;?>