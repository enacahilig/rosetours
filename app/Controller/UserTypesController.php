<?php
/*all the functions for users types are here*/
App::uses('AppController', 'Controller');
class UserTypesController  extends AppController {

	public function index(){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		$user_types = $this->UserType->find("all");
		$this->set("user_types", $user_types);
	}

	public function add(){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		if($this->request->is("post")){
			if(!$this->UserType->findByType($this->request->data["UserType"]["type"])){
				$this->UserType->create();
				$this->UserType->save($this->request->data);
				$this->Session->setFlash(__('The user type "'.$this->request->data["UserType"]["type"].'" was successfully added.'), 'default', array('class' => 'alert alert-success'));
							}
			else{
				//data already exists
				$this->Session->setFlash(__('"'.$this->request->data["UserType"]["type"].'" already exists in the system.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$this->redirect('/user_types');
	}

	public function edit($id){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		if($this->request->is("post")){
			if($this->UserType->exists($id)){
				$this->UserType->id = $id;
				$this->UserType->save($this->request->data);
				$this->Session->setFlash(__('The user type was successfully updated.'), 'default', array('class' => 'alert alert-success'));
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$this->redirect('/user_types');
	}

	public function delete($id){
		$this->loadModel("User");
		if($this->User->check_if_staff($this->Auth->user("id"))){
			$this->redirect("/users/view/{$this->Auth->user("id")}");
		}
		if($this->UserType->exists($id)){
			$this->UserType->id = $id;
			$this->UserType->delete();
			$this->Session->setFlash(__('The user type was successfully deleted.'), 'default', array('class' => 'alert alert-success'));
		}
		else{
			$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
		}
		$this->redirect('/user_types');
	}

}
;?>