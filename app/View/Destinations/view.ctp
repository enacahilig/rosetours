<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
              
                <?php echo "{$destination["Destination"]["place"]}" ;?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#editDestinationModal"));?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/destinations/delete/{$destination['Destination']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?","style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>
            </h3>
        </div>
    </div>
    <div class="row">
    
        

            
        <div class="col-md-8 col-lg-8 well">
            <p>
                <strong>Place:</strong>
                <?php echo "{$destination["Destination"]["place"]}" ;?>
            </p> 
           
            
            
            <br><br>
        </div>
    </div>
    

    <!-- Modal for Editing destination -->
    <div class="modal fade" id="editDestinationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Destination</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Destination", array("url"=>"/destinations/edit/{$destination['Destination']['id']}/from_view:1", "id"=>"DestinationEditForm"));?>
                        <div class="alert alert-info">
                            <strong>Destination Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Place</label>
                                <?php echo $this->Form->text("place", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter name of place..."));?>
                            </div>
                            
                        </div>
                       

                       
                       
                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
         </div>
    </div>
</div>