<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Destinations
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addDestinationModal"));?>
                 <?php echo $this->Html->link(__('Export Destinations'), array('action' => 'export'), array("class"=>"btn btn-primary pull-right", "style"=>"padding:3px 5px 3px 5px;font-size:12px;")); ?>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?php echo $this->Form->create('Destination', array("action"=>"search", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search for destination...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        
       
       
    </div>
    <div class="clearfix"></div>
    <br/>
    <!--Add Destination Modal-->
    <div class="modal fade" id="addDestinationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Destination</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Destination", array("url"=>"/destinations/add"));?>
                        <div class="alert alert-info">
                            <strong>Destination</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Place</label>
                                <?php echo $this->Form->text("place", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter name of Place..."));?>
                            </div>
                            
                            
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <?php if($destinations):?>
       
        <br/>

        <!--Display Destinations-->
        <div class="row">
            
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Place</th>
                            <th>Actions</th>
                          
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($destinations as $key => $destination): ?>
                            <tr>
                                <td><?php echo $destination['Destination']['id'];?></td>
                                <td><?php echo $destination['Destination']['place'];?></td>
                                
                                <td>
                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/destinations/view/{$destination["Destination"]["id"]}", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editDestinationModal", "data-id"=>$destination["Destination"]["id"], "data-place"=>$destination["Destination"]["place"]));?>
                                     <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/destinations/delete/{$destination['Destination']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            
        </div><!-- /.list of Destinations -->
    <?php else:?>
        No destination added yet.
    <?php endif;?>
    <!-- Modal for Editing Destination -->
    <div class="modal fade" id="editDestinationModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Destination</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Destination", array("url"=>"/destinations/edit", "id"=>"DestinationEditForm"));?>
                         <div class="alert alert-info">
                            <strong>Destination</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Name of Place</label>
                                <?php echo $this->Form->text("place", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter name of place..."));?>
                            </div>
                            
                        </div>

                       
                        
                        
                    </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
    


</div>  
    <!-- /.container-fluid -->
<script type="text/javascript">
    $(document).ready( function(){

        $(".editBtn").click( function   (){
            $("#DestinationEditForm #DestinationPlace").val($(this).attr("data-place"));
            
           

            $("#DestinationEditForm").attr("action", "<?php echo $this->base;?>/destinations/edit/"+$(this).attr("data-id"));
        });
    });
</script>
    