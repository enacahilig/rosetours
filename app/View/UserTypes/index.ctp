<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                User Types
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addUserTypeModal"));?>
            </h3>
        </div>
    </div>
    
    <!-- Modal for Adding User Type -->
    <div class="modal fade" id="addUserTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add User Type</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("UserType", array("url"=>"/user_types/add"));?>
                        <div class="form-group">
                            <label>Type</label>
                            <?php echo $this->Form->text("type", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter the user type..."));?>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
         </div>
    </div>
    <!-- User Types List -->
    <div class="row">
        <div class="col-lg-12">
            <?php if($user_types):?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Type</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php foreach ($user_types as $key => $user_type):?>
                                <tr>
                                    <td><?php echo $user_type["UserType"]["id"];?></td> 
                                    <td><?php echo $user_type["UserType"]["type"];?></td>
                                    <td>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editUserTypeModal", "data-id"=>$user_type["UserType"]["id"], "data-type"=>$user_type["UserType"]["type"]));?>
                                         <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/user_types/delete/{$user_type['UserType']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                    </td>
                                </tr>
                             <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- Modal for Editing User Type -->
                <div class="modal fade" id="editUserTypeModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit User Type</h4>
                            </div>
                            <div class="modal-body">
                                <?php echo $this->Form->create("UserType", array("url"=>"/user_types/edit", "id"=>"UserTypeEditForm"));?>
                                    <div class="form-group">
                                        <label>Type</label>
                                        <?php echo $this->Form->text("type", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter the user type..."));?>
                                    </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                     </div>
                </div>
            <?php else:?>
                No user types added yet.
            <?php endif;?>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
    $(document).ready( function(){
        $(".editBtn").click( function   (){
            $("#UserTypeEditForm #UserTypeType").val($(this).attr("data-type"));
            $("#UserTypeEditForm").attr("action", "<?php echo $this->base;?>/user_types/edit/" + $(this).attr("data-id"));
        });
    });
</script>
