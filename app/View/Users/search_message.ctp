<?php echo $this->Html->css("jquery-ui");?>
<?php echo $this->Html->css("jquery-ui.theme.min");?>
<?php echo $this->Html->css("jquery-ui.structure.min");?>
<div class="row">
            <div class="col-lg-12">
                    
                <?php echo $this->Html->link(__('Export Messages'), array('action' => 'export_messages'), array("class"=>"btn btn-primary pull-right")); ?>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="row">
                <?php echo $this->Form->create('User', array("action"=>"search_message", "id"=>"search_message")); ?>
                <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">

                    <label>Search</label>
                     <?php echo $this->Form->text("message", array("class"=>"form-control", "placeholder"=>"Search messages by name, type, contact number ..."));?>
                   
                 
                </div>
                <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                    <label>Pick Date</label>
                    <?php echo $this->Form->text("sent_date", array("class"=>"form-control", "id"=>"sent-date"));?>
                    
                    
               
                
                </div>
            </div>
            <div class="row">
                <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
                </div>
                    
            </div>
            
            
            <?php echo $this->Form->end(); ?>
        </div>
            
        <div class="clearfix">
            <br>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Messages </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <?php if($messages):?>
                            <table class="table table-bordered table-hover table-striped messages">
                                <thead>
                                    <tr>
                                        <th>Message</th>
                                        <th>Type</th>
                                        <th>Customer Contact no.</th>
                                        <th>Sent</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    <?php foreach ($messages as $key => $message):?>
                                    
                                    <tr>
                                        <td>
                                            <i>
                                            <?php 
                                            echo(urldecode($message['Message']['content']));
                                            ?>
                                        </i>
                                       </td>
                                        <td><?php echo ucwords($message['Message']['type']);?></td>

                                        <td><?php echo $message['Message']['customer_number'];?></td>
                                        <td><?php echo date("F d, Y h:i A", strtotime($message['Message']['sent']));?></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                            <?php else:?>
                                <div class="alert alert-info">No message found.</div>
                            <?php endif;?>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <!-- /.row -->

</div>
<!-- /.container-fluid -->
<?php echo $this->Html->script("jquery-ui");?>
<script type="text/javascript">
    $(document).ready( function(){
        $("#sent-date" ).datepicker();

        
    });
</script>
