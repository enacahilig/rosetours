<?php echo $this->Html->css("jquery-ui");?>
<?php echo $this->Html->css("jquery-ui.theme.min");?>
<?php echo $this->Html->css("jquery-ui.structure.min");?>
<div class="container-fluid">

    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Reservations
                 <?php echo $this->Html->link(__('Export Reservations'), array('action' => 'export_reservations'), array("class"=>"btn btn-primary pull-right", "style"=>"padding:3px 5px 3px 5px;font-size:12px;")); ?>
            </h3>
        </div>
    </div>  
    
    <div class="">
        
        <div class="clearfix">
            <br>
        </div>
        
        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Reservations</h3>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                        <?php if($reservations):?>
                            <div class="col-lg-12">
                                <div class="row">
                                    <?php echo $this->Form->create('User', array("action"=>"search_reservation", "id"=>"search_reservation")); ?>
                                        <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                                            <label>Search</label>
                                             <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search reservations by name, code ..."));?>
                                        </div>
                                        <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                                            <label>Pick Reservation Date</label>
                                            <?php echo $this->Form->text("date", array("class"=>"form-control", "id"=>"reserved-date"));?>
                                        </div>
                                         <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                                            <label>Pick Schedule Date</label>
                                            <?php echo $this->Form->text("sched_date", array("class"=>"form-control", "id"=>"scheduled-date"));?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                            <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
                                        </div>
                                            
                                    </div>                                
                                <?php echo $this->Form->end(); ?>
                         </div>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Code</th>
                                        <th>Destination</th>
                                        <th>Driver</th>
                                        <th>Reservation Schedule</th>
                                        <th>Date Reserved</th>
                                        <th>Schedule time</th>
                                        <th>Confirmed</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    
                                        <?php foreach ($reservations as $key => $reservation):?>
                                        <tr>
                                            <td><?php echo $reservation['Customer']['name'];?></td>
                                            <td><?php echo $reservation['Reservation']['code'];?></td>
                                            <td><?php echo $reservation['Schedule']["Destination"]['place'];?></td>
                                            <td><?php echo $reservation['Schedule']['Driver']['first_name'].' '.$reservation['Schedule']['Driver']['last_name'];?></td>
                                            <td><?php echo date("F d, Y", strtotime($reservation['Reservation']['schedule_date']));?>
                                            <td><?php echo date("F d, Y h:i A", strtotime($reservation['Reservation']['date']));?></td>
                                            <td>    <?php echo date("h:i A", strtotime($reservation['Schedule']['time']));?>
                                            </td>
                                             <td><?php
                                                if($reservation['Reservation']['confirmed']){
                                                    echo "Confirmed";
                                                    echo "&nbsp";
                                                    echo $this->Html->link("Cancel", "/users/cancel_reservation/{$reservation['Reservation']['id']}/page:1", array("class"=>"btn btn-default btn-danger", "confirm" =>"Are you sure?", "style"=>"padding:2px;"));
                                                }
                                                else{
                                                    echo "Not Confirmed";
                                                }
                                             ?>
                                             </td>
                                        </tr>
                                        <?php endforeach;?>
                                    
                                </tbody>
                            </table>
                            <?php else:?>
                                <div class="alert alert-info">No reservation found.</div>
                            <?php endif;?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.row -->

</div>
<!-- /.container-fluid -->
<?php echo $this->Html->script("jquery-ui");?>
<script type="text/javascript">
    $(document).ready( function(){
        $("#reserved-date" ).datepicker();
        $("#scheduled-date" ).datepicker();
        $("#sent-date" ).datepicker();
    });
</script>