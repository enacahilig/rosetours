<?php echo $this->Html->css("jquery-ui");?>
<?php echo $this->Html->css("jquery-ui.theme.min");?>
<?php echo $this->Html->css("jquery-ui.structure.min");?>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Dashboard <small>Statistics Overview</small>
            </h1>
        </div>
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-comments fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $avail_vans."/".$total_vans;?></div>
                            <div>Vans Available</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left"><?php echo $this->Html->link(__('View Details'),"/vans", array('class'=>''));?></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-green">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-tasks fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $total_customers;?></div>
                            <div>Customers</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left"><?php echo $this->Html->link(__('View Details'),"/vans", array('style'=>'color:#5cb85c'));?></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-yellow">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $total_reservations;?></div>
                            <div>Reserved Schedules</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left"><?php echo $this->Html->link(__('See Below'),"#", array('style'=>'color:#f0ad4e'));?></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="panel panel-red">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-support fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div class="huge"><?php echo $total_destinations;?></div>
                            <div>Destinations</div>
                        </div>
                    </div>
                </div>
                <a href="#">
                    <div class="panel-footer">
                        <span class="pull-left"><?php echo $this->Html->link(__('View Details'),"/destinations",array('style'=>'color:#d9534f'));?></span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <!-- /.row -->
    
        
    
        
    <div class="">
        <div class="row">
            <div class="col-lg-12">
               <?php echo $this->Html->link(__('Export Reservations'), array('action' => 'export_reservations'), array("class"=>"btn btn-primary pull-right")); ?>     
            </div>
        </div>
        
        <div class="clearfix">
            <br>
        </div>
        
        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-money fa-fw"></i> Reservations</h3>
                    </div>
                    <div class="panel-body">

                        <div class="table-responsive">
                        <?php if($reservations):?>
                            <div class="col-lg-12">
                                <div class="row">
                                    <?php echo $this->Form->create('User', array("action"=>"search_reservation", "id"=>"search_reservation")); ?>
                                        <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                                            <label>Search</label>
                                             <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search reservations by name, code ..."));?>
                                        </div>
                                        <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                                            <label>Pick Reservation Date</label>
                                            <?php echo $this->Form->text("date", array("class"=>"form-control", "id"=>"reserved-date"));?>
                                        </div>
                                         <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                                            <label>Pick Schedule Date</label>
                                            <?php echo $this->Form->text("sched_date", array("class"=>"form-control", "id"=>"scheduled-date"));?>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                            <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
                                        </div>
                                            
                                    </div>                                
                                <?php echo $this->Form->end(); ?>
                         </div>
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Customer Name</th>
                                        <th>Code</th>
                                        <th>Destination</th>
                                        <th>Driver</th>
                                        <th>Reservation Schedule</th>
                                        <th>Date Reserved</th>
                                        <th>Schedule time</th>
                                        <th>Confirmed</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    
                                        <?php foreach ($reservations as $key => $reservation):?>
                                        <tr>
                                            <td><?php echo $reservation['Customer']['name'];?></td>
                                            <td><?php echo $reservation['Reservation']['code'];?></td>
                                            <td><?php echo $reservation['Schedule']["Destination"]['place'];?></td>
                                            <td><?php echo $reservation['Schedule']['Driver']['first_name'].' '.$reservation['Schedule']['Driver']['last_name'];?></td>
                                            <td><?php echo date("F d, Y", strtotime($reservation['Reservation']['schedule_date']));?>
                                            <td><?php echo date("F d, Y h:i A", strtotime($reservation['Reservation']['date']));?></td>
                                            <td><?php echo date("h:i A", strtotime($reservation['Schedule']['time']));?>
                                            </td>
                                             <td><?php
                                                if($reservation['Reservation']['confirmed']){
                                                    echo "Confirmed";
                                                    echo "&nbsp";
                                                    echo $this->Html->link("Cancel", "/users/cancel_reservation/{$reservation['Reservation']['id']}", array("class"=>"btn btn-default btn-danger", "confirm" =>"Are you sure?", "style"=>"padding:2px;"));
                                                }
                                                else{
                                                    echo "Not Confirmed";
                                                }
                                             ?>
                                             </td>
                                        </tr>
                                        <?php endforeach;?>
                                    
                                </tbody>
                            </table>
                            <?php else:?>
                                <div class="alert alert-info">No reservation found.</div>
                            <?php endif;?>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="clearfix">
            
        </div>

        <div class="row">
            <div class="col-lg-12">
                    
                <?php echo $this->Html->link(__('Export Messages'), array('action' => 'export_messages'), array("class"=>"btn btn-primary pull-right")); ?>
            </div>
        </div>            
        <div class="clearfix">
            <br>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Messages </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <?php if($messages):?>
                            <div class="col-lg-12">
                                <div class="row">
                                    <?php echo $this->Form->create('User', array("action"=>"search_message", "id"=>"search_message")); ?>
                                    <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">

                                        <label>Search</label>
                                         <?php echo $this->Form->text("message", array("class"=>"form-control", "placeholder"=>"Search messages by name, type, contact number ..."));?>
                                       
                                     
                                    </div>
                                    <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                                        <label>Pick Date</label>
                                        <?php echo $this->Form->text("sent_date", array("class"=>"form-control", "id"=>"sent-date"));?>
                                        
                                        
                                   
                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                        <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
                                    </div>
                                        
                                </div>
                                
                                
                                <?php echo $this->Form->end(); ?>
                            </div>
                            <table class="table table-bordered table-hover table-striped messages">
                                <thead>
                                    <tr>
                                        <th>Message</th>
                                        <th>Type</th>
                                        <th>Customer Contact no.</th>
                                        <th>Sent</th>
                                    </tr>

                                </thead>
                                <tbody>
                                    <?php foreach ($messages as $key => $message):?>
                                    
                                    <tr>
                                        <td>
                                            <i>
                                            <?php 
                                            echo(urldecode($message['Message']['content']));
                                            ?>
                                        </i>
                                       </td>
                                        <td><?php echo ucwords($message['Message']['type']);?></td>

                                        <td><?php echo $message['Message']['customer_number'];?></td>
                                        <td><?php echo date("F d, Y h:i A", strtotime($message['Message']['sent']));?></td>
                                    </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                            <?php else:?>
                                <div class="alert alert-info">No message found.</div>
                            <?php endif;?>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>  
    </div>
    <!-- /.row -->

</div>
<!-- /.container-fluid -->
<?php echo $this->Html->script("jquery-ui");?>
<script type="text/javascript">
    $(document).ready( function(){
        $("#reserved-date" ).datepicker();
        $("#scheduled-date" ).datepicker();
        $("#sent-date" ).datepicker();
    });
</script>