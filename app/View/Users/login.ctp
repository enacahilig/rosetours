 <div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading text-center">
                    <h3 class="panel-title">Rose Tours</h3>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("User");?>
                        <fieldset>
                            <div class="form-group">
                                <?php echo $this->Form->text('username', array("class"=>"form-control", "placeholder"=>"Email"));?>
                            </div>
                            <div class="form-group">
                                <?php echo $this->Form->password('password', array("class"=>"form-control", "placeholder"=>"Password"));?>
                            </div>
                            <button type="submit" class="btn btn-lg btn-primary btn-block"><i class="glyphicon glyphicon-log-in"></i> Login</button>
                        </fieldset>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
</div>