<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                User
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addUserModal"));?>
                <?php echo $this->Html->link(__('Export Users'), array('action' => 'export_users'), array("class"=>"btn btn-primary pull-right", "style"=>"padding:3px 5px 3px 5px;font-size:12px;")); ?>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?php echo $this->Form->create('User', array("action"=>"search_user", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by id, name, status, type...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        
       
       
    </div>
    <div class="clearfix"></div>
    <br/>
    
    <!-- Modal for Adding User Type -->
    <div class="modal fade" id="addUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add User</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("User", array("url"=>"/users/add"));?>
                        <div class="alert alert-info">
                            <strong>Personal Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>First Name</label>
                                <?php echo $this->Form->text("first_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first name..."));?>
                            </div>
                             <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Last Name</label>
                                <?php echo $this->Form->text("last_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last name..."));?>
                             </div>
                        </div>

                        <div class="row">
                             <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Contact Number</label>
                                <?php echo $this->Form->text("contact_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter contact number..."));?>                             </div>
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Type</label>
                                <?php echo $this->Form->select("user_type_id", $user_types, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <?php echo $this->Form->select("status", array("1"=>"Active", "0"=>"Inactive"), array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                        </div>
                        <div class="alert alert-info">
                            <strong>Account Details</strong>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                                <?php echo $this->Form->text("username", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter user name..."));?>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <?php echo $this->Form->password("password", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter password..."));?>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
         </div>
    </div>
    <?php if($users):?>
        
        <br/>
        <!-- User Types List -->
        <div class="row">
            <div class="col-lg-12">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Contact Numer</th>
                                <th>User Type</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php foreach ($users as $key => $user):?>
                                <tr>
                                    <td><?php echo $user["User"]["id"];?></td> 
                                    <td><?php echo $user["User"]["first_name"];?></td>
                                    <td><?php echo $user["User"]["last_name"];?></td>
                                    <td><?php echo $user["User"]["contact_no"];?></td>
                                    <td><?php echo $user["UserType"]["type"];?></td>
                                    <td><?php echo $user["User"]["status"] ? "Active" : "Inactive";?></td>
                                    <td>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/users/view/{$user["User"]["id"]}", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false));?>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editUserModal", "data-id"=>$user["User"]["id"], "data-first-name"=>$user["User"]["first_name"], "data-last-name"=>$user["User"]["last_name"], "data-contact-no"=>$user["User"]["contact_no"], "data-user-type-id"=>$user["User"]["user_type_id"], "data-status"=>$user["User"]["status"], "data-username"=>$user["User"]["username"], "data-password"=>$user["User"]["password"]));?>
                                         <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/users/delete/{$user['User']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                    </td>
                                </tr>
                             <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
                <!-- Modal for Editing User Type -->
                <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                            </div>
                            <div class="modal-body">
                               <?php echo $this->Form->create("User", array("url"=>"/users/edit", "id"=>"UserEditForm"));?>
                                     <div class="alert alert-info">
                                        <strong>Personal Details</strong>
                                    </div>
                                    <div class="row">
                                        <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                            <label>First Name</label>
                                            <?php echo $this->Form->text("first_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first name..."));?>
                                        </div>
                                        <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                            <label>Last Name</label>
                                            <?php echo $this->Form->text("last_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last name..."));?>
                                         </div>
                                    </div>

                                    <div class="row">
                                         <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                            <label>Contact Number</label>
                                            <?php echo $this->Form->text("contact_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter contact number..."));?>                             </div>
                                        <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                            <label>Type</label>
                                            <?php echo $this->Form->select("user_type_id", $user_types, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Status</label>
                                        <?php echo $this->Form->select("status", array("1"=>"Active", "0"=>"Inactive"), array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                                    </div>
                                    <!-- <div class="alert alert-info">
                                        <strong>Account Details</strong>
                                    </div>
                                    <div class="form-group">
                                        <label>Username</label>
                                            <?php echo $this->Form->text("username", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter user name..."));?>
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <?php echo $this->Form->password("password", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter password...", "value"=>""));?>
                                    </div> -->
                                </div>
                            <div class="modal-footer">
                                <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                     </div>
                </div>
        
        </div>
    </div>
    <?php else:?>
        No user added yet.
    <?php endif;?>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
<script type="text/javascript">
    $(document).ready( function(){
        $(".editBtn").click( function   (){
            $("#UserEditForm #UserFirstName").val($(this).attr("data-first-name"));
            $("#UserEditForm #UserLastName").val($(this).attr("data-last-name"));
            $("#UserEditForm #UserContactNo").val($(this).attr("data-contact-no"));
            $("#UserEditForm #UserUserTypeId").val($(this).attr("data-user-type-id"));
            $("#UserEditForm #UserStatus").val($(this).attr("data-status"));
            $("#UserEditForm").attr("action", "<?php echo $this->base;?>/users/edit/" + $(this).attr("data-id"));
        });
    });
</script>
