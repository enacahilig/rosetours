<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <?php $s = substr($user["User"]["first_name"], -1) != "s" ? "s" : "" ;?>
                <?php echo "{$user["User"]["first_name"]}'{$s} Profile" ;?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#editUserModal"));?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/users/delete/{$user['User']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?","style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-lg-3 text-center">
            <?php $file = WWW_ROOT."img".DS."users" .DS.$user["User"]["id"].".jpg";?>
            <?php if(file_exists($file)):?>
                <?php echo $this->Html->image("users/{$user["User"]["id"]}.jpg", array("class"=>"img-thumbnail"));?>
            <?php else:?>
                <img src="http://placehold.it/200x200"/>
            <?php endif;?>
            
            <div class="clearfix"><br></div>
             <?php echo $this->Html->link("Change Photo <i class='glyphicon glyphicon-camera'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "data-toggle"=>"modal","data-target"=>"#uploadPhoto" ));?>

             <!-- Modal for upload photo -->
            <div class="modal fade" id="uploadPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload Photo</h4>
                      </div>
                      <div class="modal-body">
                        <form enctype="multipart/form-data" action="<?php echo $this->base;?>/users/upload_image/<?php echo $user['User']['id'] ?>" method="post">
                            <div class="form-group">
                                <input id="file" class="file" name="fileToUpload" type="file">
                            </div>
                        </form>
                        <div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <div class="col-md-8 col-lg-8 well">
            <p>
                <strong>Name:</strong>
                <?php echo "{$user["User"]["first_name"]} {$user["User"]["last_name"]}" ;?>
            </p> 
            <p>
                <strong>Contact Numer:</strong>
                <?php echo $user["User"]["contact_no"];?>
            </p>
            <p>
                <strong>Type:</strong>
                <?php echo $user["UserType"]["type"] ;?>
            </p>
            <p>
                <strong>Status:</strong>
                <?php echo $user["User"]["status"] ? "Active" : "Inactive" ;?>
            </p>
            <br><br>
        </div>
    </div>
    <!-- /.row -->
    

    <!-- Modal for Editing User Type -->
    <div class="modal fade" id="editUserModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("User", array("url"=>"/users/edit/{$user['User']['id']}/from_view:1", "id"=>"UserEditForm"));?>
                        <div class="alert alert-info">
                            <strong>Personal Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>First Name</label>
                                <?php echo $this->Form->text("first_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first name..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Last Name</label>
                                <?php echo $this->Form->text("last_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last name..."));?>
                             </div>
                        </div>

                        <div class="row">
                             <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Contact Number</label>
                                <?php echo $this->Form->text("contact_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter contact number..."));?>                             </div>
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Type</label>
                                <?php echo $this->Form->select("user_type_id", $user_types, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <?php echo $this->Form->select("status", array("1"=>"Active", "0"=>"Inactive"), array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                        </div>
                        <!-- <div class="alert alert-info">
                            <strong>Account Details</strong>
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                                <?php echo $this->Form->text("username", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter user name..."));?>
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <?php echo $this->Form->password("password", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter password...", "value"=>""));?>
                        </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
         </div>
    </div>
</div>