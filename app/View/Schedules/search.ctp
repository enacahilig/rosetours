<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Schedules
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addScheduleModal"));?>
                
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php echo $this->Form->create('Schedule', array("action"=>"search", "id"=>"search")); ?>
                <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">

                    <label>Search</label>
                     <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by plate number, driver or destination..."));?>
                   
                 
                </div>
                <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                    <label>Pick Time</label>
                    <?php echo $this->Form->text("time", array("class"=>"form-control", "id"=>"search_time"));?>
                    
                    
               
                
                </div>
            </div>
            <div class="row">
                <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
                </div>
                    
            </div>
            
            
            <?php echo $this->Form->end(); ?>
        </div>
        
       
       
    </div>
    
    <!--Add Schedule Modal-->
    <div class="modal fade" id="addScheduleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Schedule</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Schedule", array("url"=>"/Schedules/add"));?>
                        <div class="alert alert-info">
                            <strong>Schedule Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">

                                <label>Van</label>

                               <?php echo $this->Form->select("van_id", $vans, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                             
                            </div>
                           
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Driver</label>
                                <?php echo $this->Form->select("driver_id", $drivers, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                             <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Destination</label>
                                <?php echo $this->Form->select("destination_id", $destinations, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                            
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Time</label>
                                 <?php echo $this->Form->text("time", array("class"=>"form-control", "id"=>"schedule_time","required"=>true));?>
                                

                            </div>
 
       
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <?php if($schedules):?>
       
        <br/>

        <!--Display Schedules-->
        <div class="row">
            
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>User</th>
                            <th>Driver Name</th>
                            <th>Van Plate Number</th>
                            <th>Time</th>
                            <th>Destination</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($schedules as $key => $schedule): ?>
                            <tr>
                                <td><?php echo $schedule['Schedule']['id'];?></td>
                                <td><?php echo $schedule['User']['first_name'].' '.$schedule['User']['last_name'];?></td>
                                <td><?php echo $schedule['Driver']['first_name'].' '.$schedule['Driver']['last_name'];?></td>
                                <td><?php echo $schedule['Van']['plate_no'];?></td>
                                <td><?php echo date('h:i A', strtotime($schedule['Schedule']['time']));?></td>
                                <td><?php echo $schedule['Destination']['place'];?></td>
                                 <td>
                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/schedules/view/{$schedule["Schedule"]["id"]}", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editScheduleModal", "data-id"=>$schedule["Schedule"]["id"],"data-van-id"=>$schedule["Schedule"]["van_id"],"data-driver-id"=>$schedule["Schedule"]["driver_id"], "data-destination-id"=>$schedule["Schedule"]["destination_id"],"data-time"=>$schedule["Schedule"]["time"]));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/schedules/delete/{$schedule['Schedule']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            
        </div><!-- /.list of Schedules -->
    <?php else:?>
        No schedule found.
    <?php endif;?>
    <!-- Modal for Editing Schedule -->
    <div class="modal fade" id="editScheduleModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Schedule</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Schedule", array("url"=>"/schedules/edit", "id"=>"ScheduleEditForm"));?>
                         <div class="alert alert-info">
                            <strong>Schedule Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">

                                <label>Van</label>

                               <?php echo $this->Form->select("van_id", $vans, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                             
                            </div>
                           
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Driver</label>
                                <?php echo $this->Form->select("driver_id", $drivers, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                             <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Destination</label>
                                <?php echo $this->Form->select("destination_id", $destinations, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                            
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Time</label>
                                 <?php echo $this->Form->text("time", array("class"=>"form-control", "id"=>"schedule_time2","required"=>true));?>
                                

                            </div>
 
       
                        </div>

                </div>   
                        
                  
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
        


</div>  
    <!-- /.container-fluid -->
<script type="text/javascript">
    $(document).ready( function(){

        $(".editBtn").click( function   (){
            $("#ScheduleEditForm #ScheduleVanId").val($(this).attr("data-van-id"));
            $("#ScheduleEditForm #ScheduleDriverId").val($(this).attr("data-driver-id"));
            $("#ScheduleEditForm #ScheduleScheduleDestinationId").val($(this).attr("data-destination-id"));
            $("#ScheduleEditForm #ScheduleTime").val($(this).attr("data-time"));
            $("#ScheduleEditForm").attr("action", "<?php echo $this->base;?>/Schedules/edit/"+$(this).attr("data-id"));
        });
    });

            $('#schedule_time').timepicker();
            $('#schedule_time').timepicker('setTime', new Date());
            $('#schedule_time2').timepicker();
            $('#schedule_time3').timepicker();
            
            $('#search_time').timepicker();
            
            $("#time-picker").click(function(){
                $('#search_time').timepicker('show');
            });

            <?php if(isset($schedule)):?>
               $('#schedule_time2').timepicker("setTime", "<?php echo date('h:i A', strtotime($schedule['Schedule']['time']));?>");
               $('#schedule_time3').timepicker('setTime',  "<?php echo date('h:i A', strtotime($schedule['Schedule']['time']));?>");
           <?php endif;?>
     
</script>
    