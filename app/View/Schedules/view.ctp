<?php echo $this->Html->css("jquery-ui");?>
<?php echo $this->Html->css("jquery-ui.theme.min");?>
<?php echo $this->Html->css("jquery-ui.structure.min");?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Schedule
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#editCustomerModal"));?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/customers/delete/{$schedule['Schedule']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?","style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>
            </h3>
        </div>
    </div>
    <div class="row">
    
        <div class="col-md-8 col-lg-8 well">
            <p>
                <strong>Schedule ID:</strong>
                <?php echo $schedule['Schedule']['id'];?>

               
            </p> 
            <p>
                <strong>User Name:</strong>
                <?php echo $schedule['User']['first_name'].' '.$schedule['User']['last_name'];?>

               
            </p>
            <p>
                <strong>Driver's Name:</strong>
                <?php echo $schedule['Driver']['first_name'].' '.$schedule['Driver']['last_name'];?>

               
            </p> 
            <p>
                <strong>Van's Plate No:</strong>
                <?php echo $schedule['Van']['plate_no'];?>

               
            </p>
            <p>
                <strong>Date:</strong>
                <?php echo date('M d, Y', strtotime($schedule['Schedule']['date']));?>
            </p>
            <p>
                <strong>Time:</strong>
                <?php echo date('h:i A', strtotime($schedule['Schedule']['time']));?>
            </p>
            <p>
                <strong>Destination:</strong>
                <?php echo $schedule['Destination']['place'];?>

               
            </p>
            <br><br>
        </div>
    </div>
    

    <!-- Modal for Editing customer -->
    <div class="modal fade" id="editCustomerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Schedule</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Schedule", array("url"=>"/schedules/edit/{$schedule['Schedule']['id']}/from_view:1", "id"=>"ScheduleEditForm"));?>
                         <div class="alert alert-info">
                            <strong>Schedule Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">

                                <label>Van</label>

                               <?php echo $this->Form->select("van_id", $vans, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                             
                            </div>
                           
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Driver</label>
                                <?php echo $this->Form->select("driver_id", $drivers, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                             <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Destination</label>
                                <?php echo $this->Form->select("destination_id", $destinations, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Date</label>
                                 <?php echo $this->Form->text("date", array("class"=>"form-control", "id"=>"date1","required"=>true));?>
                                

                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Time</label>
                                 <?php echo $this->Form->text("time", array("class"=>"form-control", "id"=>"schedule_time3","required"=>true));?>
                                

                            </div>
 
       
                        </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
         </div>
    </div>
</div>
<?php echo $this->Html->script("jquery-ui");?>
<script type="text/javascript">
    $('#schedule_time3').timepicker();
     $('#date1').datepicker();
</script>