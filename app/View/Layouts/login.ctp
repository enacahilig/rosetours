<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    
    <title>Rose Tours</title>

    <!-- Bootstrap Core CSS -->
    <?php echo $this->Html->css("bootstrap.min");?>

    <!-- Custom CSS -->
    <?php echo $this->Html->css("sb-admin");?>

    <!-- Morris Charts CSS -->
     <?php echo $this->Html->css("plugins/morris");?>

    <?php echo $this->Html->script("jquery");?>
</head>

<body>
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4"> 
            <?php echo $this->Session->flash(); ?>
        </div>
    </div>
        <?php echo $this->fetch('content'); ?>
    
</body>

</html>
