<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->meta(
        'favicon.ico',
        '/favicon.ico',
        array('type' => 'icon')
    );
    ?>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
	
    <title>Rose Tours</title>

    <!-- Bootstrap Core CSS -->
    <?php echo $this->Html->css("bootstrap.min");?>

    <!-- Custom CSS -->
    <?php echo $this->Html->css("sb-admin");?>

    <!-- Morris Charts CSS -->
     <?php echo $this->Html->css("plugins/morris");?>

    <?php echo $this->Html->script("jquery");?>
   
     <?php echo $this->Html->css("jquery.timepicker");?>
     
     
    <?php echo $this->Html->script("jquery.timepicker.min");?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
	 <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo $this->base;?>">Rose Tours</a>
            </div>
            
            <!-- Top Menu Items -->
            <?php if( $logged_user ):?>
                <ul class="nav navbar-right top-nav">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-home"></i> <?php echo $logged_user["User"]["username"];?> <b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li>
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon-user'></i>  Profile", "/users/view/{$logged_user["User"]["id"]}", array("escape"=>false));?>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon-cog'></i> Settings", "#", array("data-toggle"=>"modal", "data-target"=>"#settingsModal", "escape"=>false));?>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon-log-out'></i> Log Out", "/users/logout", array("escape"=>false));?>
                            </li>
                        </ul>
                    </li>
                </ul>
                
               <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav side-nav">
                        <li class="<?php echo $this->params['controller']=='users' &&  $this->params['action']=='dashboard' ? 'active' : '';?>">
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon-dashboard'></i> Dashboard", "/users/dashboard", array("escape"=>false));?>
                        </li>
                        <?php if( strtolower($logged_user["UserType"]["type"])=="manager"):?>
                            <li class="<?php echo $this->params['controller']=='users' &&  ($this->params['action']=='index' || $this->params['action']=='search') ? 'active' : '';?>">
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i> Users", "/users", array("escape"=>false));?>
                            </li>
                            <li class="<?php echo $this->params['controller']=='user_types' &&  $this->params['action']=='index' ? 'active' : '';?>">
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon glyphicon-bookmark'></i> User Types", "/user_types/", array("escape"=>false));?>
                            </li>
                        <?php endif;?>
                        <li class="<?php echo $this->params['controller']=='customers' &&  ($this->params['action']=='index' || $this->params['action']=='search') ? 'active' : '';?>">
                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-user'></i> Customers", "/customers", array("escape"=>false));?>
                        </li>
                        <li class="<?php echo $this->params['controller']=='vans' &&  ($this->params['action']=='index' || $this->params['action']=='search') ? 'active' : '';?>">
                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-bed'></i> Vans", "/vans", array("escape"=>false));?>
                        </li> 
                        <li class="<?php echo $this->params['controller']=='drivers' &&  ($this->params['action']=='index' || $this->params['action']=='search') ? 'active' : '';?>">
                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-user'></i> Drivers", "/drivers", array("escape"=>false));?>
                        </li>
                        <li class="<?php echo $this->params['controller']=='destinations' &&  ($this->params['action']=='index' || $this->params['action']=='search') ? 'active' : '';?>">
                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-road'></i> Destinations", "/destinations", array("escape"=>false));?>
                        </li>
                        <li class="<?php echo $this->params['controller']=='schedules' &&  ($this->params['action']=='index' || $this->params['action']=='search') ? 'active' : '';?>">
                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-calendar'></i> Schedules", "/schedules", array("escape"=>false));?>
                        </li>
                        <li class="<?php echo $this->params['controller']=='users' &&  ($this->params['action']=='reservations' || $this->params['action']=='search') ? 'active' : '';?>">
                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i> Reservations", "/users/reservations", array("escape"=>false));?>
                        </li>
                         <li class="<?php echo $this->params['controller']=='users' &&  ($this->params['action']=='messages' || $this->params['action']=='search') ? 'active' : '';?>">
                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-envelope'></i> Messages", "/users/messages", array("escape"=>false));?>
                        </li>
                    </ul>
                </div>
             <?php endif;?>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">
        	<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
        </div>
        <!-- /#page-wrapper -->

        <!-- Modal for Settings of User -->
        <div class="modal fade" id="settingsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Settings</h4>
                    </div>
                    <div class="modal-body">
                       <?php echo $this->Form->create("User", array("url"=>"/users/settings"));?>
                            <div class="alert alert-info">
                                <strong>Account Details</strong>
                            </div>
                            <div class="form-group">
                                <label>Username</label>
                                    <?php echo $this->Form->text("username", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter user name...", "value"=>$logged_user["User"]["username"]));?>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <?php echo $this->Form->password("password", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter password...", "value"=>""));?>
                            </div>
                        </div>
                    <div class="modal-footer">
                        <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                        <?php echo $this->Form->end();?>
                    </div>
                </div>
             </div>
        </div>

    </div>
    <!-- /#wrapper -->
     <!-- jQuery -->
    <?php echo $this->Html->script("bootstrap.min");?>
    <?php echo $this->Html->script("plugins/morris/raphael.min");?>
    <?php echo $this->Html->script("plugins/morris/morris.min");?>
    <?php echo $this->Html->script("plugins/morris/morris-data");?>	
</body>
</html>
