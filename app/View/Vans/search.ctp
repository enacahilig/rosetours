<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Vans
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addVanModal"));?>
            </h3>
        </div>
        
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?php echo $this->Form->create('Van', array("action"=>"search", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by plate number, capacity or status...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        
       
       
    </div>
    <div class="clearfix"></div>
    <br/>
    <!--Add Van Modal-->
    <div class="modal fade" id="addVanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Van</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Van", array("url"=>"/vans/add"));?>
                        <div class="alert alert-info">
                            <strong>Van Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Plate Number</label>
                                <?php echo $this->Form->text("plate_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter plate number..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Status</label>
                                <?php echo $this->Form->text("status", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter status..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Capacity</label>
                                <?php echo $this->Form->number("capacity", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter capacity..."));?>
                            </div>
                            
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <?php if($vans):?>
       
        <br/>
        <!--Display Vans-->
        <div class="row">
            
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Plate Number</th>
                            <th>Status</th>
                            <th>Capacity</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($vans as $key => $van): ?>
                            <tr>
                                <td><?php echo $van['Van']['id'];?></td>
                                <td><?php echo $van['Van']['plate_no'];?></td>
                                <td><?php echo $van['Van']['status'];?></td>
                                <td><?php echo $van['Van']['capacity'];?></td>
                                <td>
                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/vans/view/{$van["Van"]["id"]}", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editVanModal", "data-id"=>$van["Van"]["id"], "data-plate-no"=>$van["Van"]["plate_no"], "data-status"=>$van["Van"]["status"],"data-capacity"=>$van["Van"]["capacity"]));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/vans/delete/{$van['Van']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            
        </div><!-- /.list of Vans -->
    <?php else:?>
        No van found.
    <?php endif;?>
    <!-- Modal for Editing Van -->
    <div class="modal fade" id="editVanModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Van</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Van", array("url"=>"/vans/edit", "id"=>"VanEditForm"));?>
                         <div class="alert alert-info">
                            <strong>Van Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Plate Number</label>
                                <?php echo $this->Form->text("plate_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter plate number..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Status</label>
                                <?php echo $this->Form->text("status", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter van status..."));?>
                             </div>
                        </div>
                        <div class="row">
                             <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Capacity</label>
                                <?php echo $this->Form->number("capacity", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter van capacity..."));?>
                             </div>
                            
                        </div>

                       
                        
                        
                    </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
    


</div>  
    <!-- /.container-fluid -->
<script type="text/javascript">
    $(document).ready( function(){

        $(".editBtn").click( function   (){
            $("#VanEditForm #VanPlateNo").val($(this).attr("data-plate-no"));
            $("#VanEditForm #VanStatus").val($(this).attr("data-status"));
            $("#VanEditForm #VanCapacity").val($(this).attr("data-capacity"));
           

            $("#VanEditForm").attr("action", "<?php echo $this->base;?>/vans/edit/"+$(this).attr("data-id"));
        });
        
    });
</script>
    