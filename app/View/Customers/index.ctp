<?php echo $this->Html->css("jquery-ui");?>
<?php echo $this->Html->css("jquery-ui.theme.min");?>
<?php echo $this->Html->css("jquery-ui.structure.min");?>

<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Customers
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addCustomerModal"));?>
                 <?php echo $this->Html->link(__('Export Customers'), array('action' => 'export'), array("class"=>"btn btn-primary pull-right", "style"=>"padding:3px 5px 3px 5px;font-size:12px;")); ?>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="row">
                <?php echo $this->Form->create('Customer', array("action"=>"search", "id"=>"search")); ?>
                <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">

                    <label>Search</label>
                     <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by name, contact number, code..."));?>
                   
                 
                </div>
                <div class="form-group  col-sm-4 col-md-4 col-lg-4 ">
                    <label>Pick Date</label>
                    <?php echo $this->Form->text("date", array("class"=>"form-control", "id"=>"datepicker"));?>
                    
                    
               
                
                </div>
            </div>
            <div class="row">
                <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
                </div>
                    
            </div>
            
            
            <?php echo $this->Form->end(); ?>
        </div>
        
       
       
    </div>
    <!--Add Customer Modal-->
    <div class="modal fade" id="addCustomerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Customer</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Customer", array("url"=>"/customers/add"));?>
                        <div class="alert alert-info">
                            <strong>Customer Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Name</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter customer's name..."));?>
                            </div>
                           
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Code</label>
                                <?php echo $this->Form->text("code", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter code..."));?>
                            </div>

                            
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Contact Number</label>
                                <?php echo $this->Form->number("contact_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter contact number..."));?>
                            </div>
                            <?php echo $this->Form->text("date", array("value"=>date("Y-m-d H:i:s"), "class"=>"hide"));?>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <?php if($customers):?>
     
    <br>    
        
    <!--Display Customers-->
        <div class="row">
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <!-- <th>Code</th> -->
                            <th>Bad Record</th>
                            <th>Contact Number</th>
                            <th>Date Added</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($customers as $key => $customer): ?>
                            <tr>
                                <td><?php echo $customer['Customer']['id'];?></td>
                                <td><?php echo $customer['Customer']['name'];?></td>
                                <!-- <td><?php echo $customer['Customer']['code'];?></td> -->
                                <td><?php echo $customer['Customer']['bad_records_count'];?></td>
                                <td><?php echo $customer['Customer']['contact_no'];?></td>
                                <td><?php echo date("F d, Y h:i A", strtotime($customer['Customer']['date']));?></td>
                                <td>
                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/customers/view/{$customer["Customer"]["id"]}", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editCustomerModal", "data-id"=>$customer["Customer"]["id"], "data-name"=>$customer["Customer"]["name"], "data-code"=>$customer["Customer"]["code"], "data-bad-records-count"=>$customer["Customer"]["bad_records_count"], "data-contact-no"=>$customer["Customer"]["contact_no"]));?>
                                     <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/customers/delete/{$customer['Customer']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            
        </div><!-- /.list of customers -->
    <?php else:?>
        No customer added yet.
    <?php endif;?>
    <!-- Modal for Editing Customer -->
    <div class="modal fade" id="editCustomerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Customer</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Customer", array("url"=>"/customers/edit", "id"=>"CustomerEditForm"));?>
                         <div class="alert alert-info">
                            <strong>Customer Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Name</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter customer's name..."));?>
                            </div>
                           
                        </div>

                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Code</label>
                                <?php echo $this->Form->text("code", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter code..."));?>   
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Contact Number</label>
                                <?php echo $this->Form->text("contact_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter contact number..."));?>   
                            </div>
                        </div>
                        
                        
                    </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
    


</div>  
    <!-- /.container-fluid -->
<?php echo $this->Html->script("jquery-ui");?>
<script type="text/javascript">
    $(document).ready( function(){
        $("#datepicker" ).datepicker();
        $(".editBtn").click( function   (){
            $("#CustomerEditForm #CustomerName").val($(this).attr("data-name"));
            $("#CustomerEditForm #CustomerCode").val($(this).attr("data-code"));
            $("#CustomerEditForm #CustomerBadRecordsCount").val($(this).attr("data-bad-records-count"));
            $("#CustomerEditForm #CustomerContactNo").val($(this).attr("data-contact-no"));
            $("#CustomerEditForm").attr("action", "<?php echo $this->base;?>/customers/edit/"+$(this).attr("data-id"));
        });
    });
</script>
    