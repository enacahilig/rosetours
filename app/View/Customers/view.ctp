<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <?php $s = substr($customer["Customer"]["name"], -1) != "s" ? "s" : "" ;?>
                <?php echo "{$customer["Customer"]["name"]}'{$s} Profile" ;?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#editCustomerModal"));?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/customers/delete/{$customer['Customer']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?","style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>
            </h3>
        </div>
    </div>
    <div class="row">
    
        <div class="col-md-3 col-lg-3 text-center">
            <?php $file = WWW_ROOT."img".DS."customers" .DS.$customer["Customer"]["id"].".jpg";?>
            <?php if(file_exists($file)):?>
                <?php echo $this->Html->image("customers/{$customer["Customer"]["id"]}.jpg", array("class"=>"img-thumbnail"));?>
            <?php else:?>
                <img src="http://placehold.it/200x200"/>
            <?php endif;?>
            
            <div class="clearfix"><br></div>
             <?php echo $this->Html->link("Change Photo <i class='glyphicon glyphicon-camera'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "data-toggle"=>"modal","data-target"=>"#uploadPhoto" ));?>

             <!-- Modal for upload photo -->
            <div class="modal fade" id="uploadPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload Photo</h4>
                      </div>
                      <div class="modal-body">
                        <form enctype="multipart/form-data" action="<?php echo $this->base;?>/customers/upload_image/<?php echo $customer['Customer']['id'] ?>" method="post">
                            <div class="form-group">
                                <input id="file" class="file" name="fileToUpload" type="file">
                            </div>
                        </form>
                        <div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <div class="col-md-8 col-lg-8 well">
            <p>
                <strong>Name:</strong>
                <?php echo "{$customer["Customer"]["name"]}" ;?>
            </p> 
            <p>
                <strong>Code:</strong>
                <?php echo $customer["Customer"]["code"];?>
            </p>
            <p>
                <strong>Bad Records:</strong>
                <?php echo $customer["Customer"]["bad_records_count"] ;?>
            </p>
             <p>
                <strong>Contact Number:</strong>
                <?php echo $customer["Customer"]["contact_no"] ;?>
            </p>
            
            <br><br>
        </div>
    </div>
    

    <!-- Modal for Editing customer -->
    <div class="modal fade" id="editCustomerModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit User</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Customer", array("url"=>"/customers/edit/{$customer['Customer']['id']}/from_view:1", "id"=>"CustomerEditForm"));?>
                        <div class="alert alert-info">
                            <strong>Customer Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-12 col-md-12 col-lg-12 ">
                                <label>Name</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter customer's name..."));?>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Code</label>
                                <?php echo $this->Form->text("code", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter code..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Bad Records</label>
                                <?php echo $this->Form->number("bad_records_count", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter number of bad records..."));?>
                             </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Contact Number</label>
                                <?php echo $this->Form->text("contact_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter contact number..."));?>
                            </div>

                        </div>

                       
                       
                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
         </div>
    </div>
</div>