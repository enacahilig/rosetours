<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>
<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                <?php $s = substr($driver["Driver"]["first_name"], -1) != "s" ? "s" : "" ;?>
                <?php echo "{$driver["Driver"]["first_name"]}'{$s} Profile" ;?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#editDriverModal"));?>
                <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/drivers/delete/{$driver['Driver']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?","style"=>"padding:3px 5px 3px 5px;font-size:12px;" ));?>
            </h3>
        </div>
    </div>
    <div class="row">
    
        <div class="col-md-3 col-lg-3 text-center">
            <?php $file = WWW_ROOT."img".DS."drivers" .DS.$driver["Driver"]["id"].".jpg";?>
            <?php if(file_exists($file)):?>
                <?php echo $this->Html->image("drivers/{$driver["Driver"]["id"]}.jpg", array("class"=>"img-thumbnail"));?>
            <?php else:?>
                <img src="http://placehold.it/200x200"/>
            <?php endif;?>
            
            <div class="clearfix"><br></div>
             <?php echo $this->Html->link("Change Photo <i class='glyphicon glyphicon-camera'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "data-toggle"=>"modal","data-target"=>"#uploadPhoto" ));?>

             <!-- Modal for upload photo -->
            <div class="modal fade" id="uploadPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Upload Photo</h4>
                      </div>
                      <div class="modal-body">
                        <form enctype="multipart/form-data" action="<?php echo $this->base;?>/drivers/upload_image/<?php echo $driver['Driver']['id'] ?>" method="post">
                            <div class="form-group">
                                <input id="file" class="file" name="fileToUpload" type="file">
                            </div>
                        </form>
                        <div>
                        </div>
                      </div>
                    </div>
                  </div>
            </div>
        </div>
        <div class="col-md-8 col-lg-8 well">
            <p>
                <strong>Name:</strong>
                <?php echo "{$driver["Driver"]["first_name"]} {$driver["Driver"]["last_name"]}" ;?>
            </p> 
            <p>
                <strong>License Number:</strong>
                <?php echo $driver["Driver"]["license_no"];?>
            </p>
            
            
            <br><br>
        </div>
    </div>
    

    <!-- Modal for edit Driver Type -->
    <div class="modal fade" id="editDriverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Driver</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Driver", array("url"=>"/drivers/edit/{$driver['Driver']['id']}/from_view:1", "id"=>"DriverEditForm"));?>
                        <div class="alert alert-info">
                            <strong>Driver Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>First Name</label>
                                <?php echo $this->Form->text("first_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first name..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Last Name</label>
                                <?php echo $this->Form->text("last_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last name..."));?>
                             </div>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>License Number</label>
                                <?php echo $this->Form->text("license_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter license number..."));?>
                            </div>
                           
                        </div>
                        

                       
                       
                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
         </div>
    </div>
    <div class="clearfix"><br></div>
    <div class="clearfix"><br></div>
        <div class="row">

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="page-header"><i class="fa fa-money fa-fw"></i><?php echo "{$driver["Driver"]["first_name"]} {$driver["Driver"]["last_name"]}" ;?> Feedback </h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Customer</th>
                                        <th>Message</th>
                                    </tr>
                                </thead>
                                <?php foreach($driver["Feedback"] as $index => $feedback):?>
                                    <tbody>
                                       
                                            <tr>
                                                <td><?php echo $feedback['Customer']['name'];?></td>
                                                <td><?php echo $feedback['comment'];?></td>
                                            </tr>
                                       
                                    </tbody>
                                <?php endforeach;?>
                            </table>
                            
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="page-header"><i class="fa fa-money fa-fw"></i><?php echo "{$driver["Driver"]["first_name"]} {$driver["Driver"]["last_name"]}" ;?> Rewards <?php echo $this->Html->link("<i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addRewardsModal"));?></h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            
                               
                            
                            <table class="table table-bordered table-hover table-striped">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Amount (Php)</th>
                                    </tr>
                                </thead>
                                <?php foreach($rewards as $key => $reward):?>
                                <tbody>
                                   
                                        <tr>
                                            <td><?php echo $reward['Reward']['name'];?></td>
                                            <td><?php echo $reward['Reward']['amount'];?></td>
                                        </tr>
                                   
                                </tbody>
                                
                                <?php endforeach;?>
                                <tfoot>
                                    <tr>
                                        <td><b>Total Rewards:</b></td>
                                        <?php
                                            $total=0;
                                            foreach ($rewards as $key => $reward) {
                                                $total+=$reward['Reward']['amount'];
                                            }
                                            
                                        ?>
                                        <td><?php echo $total;?></td>
                                    </tr>
                                </tfoot>
                            </table>
                            
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
        <div class="modal fade" id="addRewardsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Rewards to this Driver</h4>
                </div>
                <div class="modal-body">

                    <?php echo $this->Form->create("Reward", array("url"=>"/drivers/add_rewards/{$driver['Driver']['id']}/from_view:1"));?>
                        <div class="alert alert-info">
                            <strong>Reward's Details</strong>
                        </div>
                        <div class="row">
                            <?php echo $this->Form->text("driver_id", array("class"=>"form-control", "style"=>"display:none","value" => "{$driver['Driver']['id']}"));?>
                           

                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Name</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter reward's name..."));?>
                            </div>
                            
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Amount</label>
                                <?php echo $this->Form->text("amount", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter amount..."));?>
                            </div>
                            
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>


</div>