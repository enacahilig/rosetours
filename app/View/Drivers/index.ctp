<div class="container-fluid">

    <!-- Page Heading -->
    <div class="row">
        <div class="col-lg-12">
            <h3 class="page-header">
                Drivers
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "#", array("class"=>"btn btn-default btn-primary", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal", "data-target"=>"#addDriverModal"));?>
                <?php echo $this->Html->link(__('Export Drivers'), array('action' => 'export'), array("class"=>"btn btn-primary pull-right", "style"=>"padding:3px 5px 3px 5px;font-size:12px;")); ?>
            </h3>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <?php echo $this->Form->create('Driver', array("action"=>"search", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Search by name or by license no...", "required"=>true));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
    
    <div class="clearfix"></div>
    <br/>
    <!--Add Driver Modal-->
    <div class="modal fade" id="addDriverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Driver</h4>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create("Driver", array("url"=>"/drivers/add"));?>
                        <div class="alert alert-info">
                            <strong>Driver Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>First Name</label>
                                <?php echo $this->Form->text("first_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first name..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Last Name</label>
                                <?php echo $this->Form->text("last_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last name..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>License Number</label>
                                <?php echo $this->Form->text("license_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter license no..."));?>
                            </div>
                            
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-success"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>
    <?php if($drivers):?>
        
        <br/>
        <!--Display Drivers-->
        <div class="row">
            
            <div class="table-responsive">
                <table class="table table-bordered table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>License Number</th>
                            <th>Action</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($drivers as $key => $driver): ?>
                            <tr>
                                <td><?php echo $driver['Driver']['id'];?></td>
                                <td><?php echo $driver['Driver']['first_name'];?></td>
                                <td><?php echo $driver['Driver']['last_name'];?></td>
                                <td><?php echo $driver['Driver']['license_no'];?></td>
                                <td>
                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-folder-open'></i>", "/drivers/view/{$driver["Driver"]["id"]}", array("class"=>"btn btn-default btn-primary editBtn addRewards", "escape"=>false));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "#", array("class"=>"btn btn-default btn-primary editBtn", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#editDriverModal", "data-id"=>$driver["Driver"]["id"], "data-first-name"=>$driver["Driver"]["first_name"], "data-last-name"=>$driver["Driver"]["last_name"], "data-license-no"=>$driver["Driver"]["license_no"]));?>
                                     <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/drivers/delete/{$driver['Driver']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                     <?php echo $this->Html->link("<i class='glyphicon glyphicon-gift'></i>", "#", array("class"=>"btn btn-default btn-primary addRewards", "escape"=>false, "data-toggle"=>"modal", "data-target"=>"#addRewardsModal"));?>

                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            
        </div><!-- /.list of drivers -->
    <?php else:?>
        No driver added yet.
    <?php endif;?>
    <!-- Modal for Editing Driver -->
    <div class="modal fade" id="editDriverModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Edit Driver</h4>
                </div>
                <div class="modal-body">
                   <?php echo $this->Form->create("Driver", array("url"=>"/drivers/edit", "id"=>"DriverEditForm"));?>
                         <div class="alert alert-info">
                            <strong>Driver Details</strong>
                        </div>
                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>First Name</label>
                                <?php echo $this->Form->text("first_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter first name..."));?>
                            </div>
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Last Name</label>
                                <?php echo $this->Form->text("last_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter last name..."));?>
                             </div>
                        </div>

                        <div class="row">
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>License Number</label>
                                <?php echo $this->Form->text("license_no", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter license no..."));?>   
                            </div>
                            
                        </div>
                       
                        
                        
                    </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div><!-- /.modal -->
        <!--Add Rewards Modal-->
    <div class="modal fade" id="addRewardsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Rewards to this Driver</h4>
                </div>
                <div class="modal-body">

                    <?php echo $this->Form->create("Reward", array("url"=>"/drivers/add_rewards/{$driver['Driver']['id']}"));?>
                        <div class="alert alert-info">
                            <strong>Reward's Details</strong>
                        </div>
                        <div class="row">
                            <?php echo $this->Form->text("driver_id", array("class"=>"form-control", "style"=>"display:none","value" => "{$driver['Driver']['id']}"));?>
                           

                            <div class="form-group  col-sm-6 col-md-6 col-lg-6 ">
                                <label>Name</label>
                                <?php echo $this->Form->text("name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter reward's name..."));?>
                            </div>
                            
                            <div class="form-group  col-sm-6 col-md-6  col-lg-6">
                                <label>Amount</label>
                                <?php echo $this->Form->text("amount", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter amount..."));?>
                            </div>
                            
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button submit" class="btn btn-primary"><i class="glyphicon glyphicon-check"></i>Save</button>
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
    </div>


    


</div>  
    <!-- /.container-fluid -->
<script type="text/javascript">
    $(document).ready( function(){

        $(".editBtn").click( function   (){
            $("#DriverEditForm #DriverFirstName").val($(this).attr("data-first-name"));
            $("#DriverEditForm #DriverLastName").val($(this).attr("data-last-name"));
            $("#DriverEditForm #DriverLicenseNo").val($(this).attr("data-license-no"));
            
            $("#DriverEditForm").attr("action", "<?php echo $this->base;?>/drivers/edit/"+$(this).attr("data-id"));
        });

    });
</script>
    