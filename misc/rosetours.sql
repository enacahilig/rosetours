-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 05, 2016 at 12:07 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `rosetours`
--

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE IF NOT EXISTS `customers` (
  `id` int(30) NOT NULL,
  `code` varchar(10) NOT NULL,
  `bad_records_count` int(10) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `name` varchar(100) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `code`, `bad_records_count`, `contact_no`, `name`, `date`) VALUES
(1, '1234', 0, '0910465', 'Ena Fleurence', '0000-00-00 00:00:00'),
(2, '65656', 1, '265463', 'Fleurence', '0000-00-00 00:00:00'),
(3, '', 0, '123', 'ENA CAHILIG', '0000-00-00 00:00:00'),
(5, '', 1, '12312389', 'A CAHILIG', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `destinations`
--

CREATE TABLE IF NOT EXISTS `destinations` (
  `id` int(11) NOT NULL,
  `place` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `destinations`
--

INSERT INTO `destinations` (`id`, `place`) VALUES
(2, 'Tumbukthree'),
(3, 'Iloilo');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE IF NOT EXISTS `drivers` (
  `id` int(10) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `license_no` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `first_name`, `last_name`, `license_no`) VALUES
(2, 'Ben', 'Ten', 4524),
(3, 'San', 'Diego', 5342);

-- --------------------------------------------------------

--
-- Table structure for table `feedbacks`
--

CREATE TABLE IF NOT EXISTS `feedbacks` (
  `id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedbacks`
--

INSERT INTO `feedbacks` (`id`, `driver_id`, `comment`, `customer_id`) VALUES
(1, 2, 'sdfsdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL,
  `message_id` varchar(50) NOT NULL,
  `content` varchar(100) NOT NULL,
  `type` varchar(50) NOT NULL,
  `customer_number` varchar(50) NOT NULL,
  `delivered` tinyint(4) NOT NULL,
  `modified` datetime NOT NULL,
  `sent` datetime NOT NULL,
  `request_id` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message_id`, `content`, `type`, `customer_number`, `delivered`, `modified`, `sent`, `request_id`) VALUES
(1, 'VINA1TIqir', 'Send+Message%21', 'outgoing', '639124136476', 0, '2015-08-22 08:39:33', '2015-08-22 08:39:33', '0'),
(2, 'GmutGiyswPP', 'This is a sample text yeah baby!', 'outgoing', '639124136476', 0, '2015-08-22 09:02:41', '2015-08-22 09:02:41', '0'),
(3, 'N5hRqZMn18', 'You+have+successfully+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd-', 'outgoing', '0', 0, '2015-08-26 15:04:57', '2015-08-26 15:04:57', ''),
(4, 'DjHIEB2tcK', 'This+number+is+already+registered+in+Rose+Tours.', 'outgoing', '0', 0, '2015-08-26 15:05:06', '2015-08-26 15:05:06', ''),
(5, '7C6HySJWrs', 'This+number+is+already+registered+in+Rose+Tours.', 'outgoing', '0', 0, '2015-08-26 15:05:07', '2015-08-26 15:05:07', ''),
(6, '7E7AoaHImg', 'This+number+is+already+registered+in+Rose+Tours.', 'outgoing', '265463', 0, '2015-08-26 15:06:38', '2015-08-26 15:06:38', ''),
(7, '4kBEAVey6r', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:08:59', '2015-08-26 15:08:59', ''),
(8, 'QHWnvhYFQ2', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:09:54', '2015-08-26 15:09:54', ''),
(9, 'vOWbqLnr20', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:09:55', '2015-08-26 15:09:55', ''),
(10, 'g0dtbDhGew', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:10:44', '2015-08-26 15:10:44', ''),
(11, '5y0oYBBsNL', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:19', '2015-08-26 15:11:19', ''),
(12, 'BsTv7A6PJz', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:20', '2015-08-26 15:11:20', ''),
(13, 'LaxDJv7ulj', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:21', '2015-08-26 15:11:21', ''),
(14, 'BmryyYoH4w', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:22', '2015-08-26 15:11:22', ''),
(15, '0IKKD5FR9K', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:22', '2015-08-26 15:11:22', ''),
(16, 'sARcb8Cpre', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:22', '2015-08-26 15:11:22', ''),
(17, 'MOl5bqsqaF', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:23', '2015-08-26 15:11:23', ''),
(18, 'UsAyfVLwB1', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:58', '2015-08-26 15:11:58', ''),
(19, 'zyztk7dvTa', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:11:59', '2015-08-26 15:11:59', ''),
(20, '9QBcsGiaKK', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:12:00', '2015-08-26 15:12:00', ''),
(21, 'kw7hxg8YhQ', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:12:00', '2015-08-26 15:12:00', ''),
(22, 'U3ne2fbUCL', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Byyyy-', 'outgoing', '265463', 0, '2015-08-26 15:12:16', '2015-08-26 15:12:16', ''),
(23, 'xPdjNsX9MM', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:12:48', '2015-08-26 15:12:48', ''),
(24, 'qXRfHXSQoT', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:12:50', '2015-08-26 15:12:50', ''),
(25, '92BXxMTSoz', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:15:31', '2015-08-26 15:15:31', ''),
(26, 'oSylFGmAPu', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:15:32', '2015-08-26 15:15:32', ''),
(27, 'Egs1MQFwAj', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:16:28', '2015-08-26 15:16:28', ''),
(28, '2Gp6EJ6d7o', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:17:07', '2015-08-26 15:17:07', ''),
(29, 'Z6xSWKuUiA', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:17:08', '2015-08-26 15:17:08', ''),
(30, '4HTDSZ3k0X', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:17:12', '2015-08-26 15:17:12', ''),
(31, 'FD2m235o1w', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:18:19', '2015-08-26 15:18:19', ''),
(32, 'JMYw4LI0G1', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:18:29', '2015-08-26 15:18:29', ''),
(33, 'CtHNvSFOIG', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:18:33', '2015-08-26 15:18:33', ''),
(34, 'Ig7RqU2STV', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:18:39', '2015-08-26 15:18:39', ''),
(35, 'jHI3jOYvRa', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:22:34', '2015-08-26 15:22:34', ''),
(36, 'g0ieGLuZQv', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:22:34', '2015-08-26 15:22:34', ''),
(37, 'KUOsWWbkpz', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:23:08', '2015-08-26 15:23:08', ''),
(38, '4kZK4b2Yf8', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:23:08', '2015-08-26 15:23:08', ''),
(39, 'nbdRmR3BQZ', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:23:29', '2015-08-26 15:23:29', ''),
(40, 'n5a0hw9TkC', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:23:29', '2015-08-26 15:23:29', ''),
(41, 'a8DB2GNc80', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:24:59', '2015-08-26 15:24:59', ''),
(42, 'TDg5xBOnOk', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:24:59', '2015-08-26 15:24:59', ''),
(43, 'AQixWY7sJJ', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:25:40', '2015-08-26 15:25:40', ''),
(44, 'QNizzLtuT6', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:25:40', '2015-08-26 15:25:40', ''),
(45, 'BVV0xRecCK', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:26:12', '2015-08-26 15:26:12', ''),
(46, 'VVHAAR6Pkg', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:26:12', '2015-08-26 15:26:12', ''),
(47, 'GrIPNTSNga', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:26:54', '2015-08-26 15:26:54', ''),
(48, 'nRsIkfbvjG', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:26:54', '2015-08-26 15:26:54', ''),
(49, 'O0qobcg4Dy', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:27:02', '2015-08-26 15:27:02', ''),
(50, 'UX11ZLF5yH', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:27:02', '2015-08-26 15:27:02', ''),
(51, 'rIevFn6nc1', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:28:19', '2015-08-26 15:28:19', ''),
(52, 'K2n8iJ3Hi9', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:28:19', '2015-08-26 15:28:19', ''),
(53, 'y9SwJuCyVa', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:28:52', '2015-08-26 15:28:52', ''),
(54, 'CtZ9s0gUfn', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:28:52', '2015-08-26 15:28:52', ''),
(55, 'Bqzhj9xWh4', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:29:02', '2015-08-26 15:29:02', ''),
(56, 'I6PDdXOPDe', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:29:02', '2015-08-26 15:29:02', ''),
(57, 'y638Vr4pfk', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:29:14', '2015-08-26 15:29:14', ''),
(58, 'MfmHMoD292', 'This+number+is+already+registered+in+Rose+Tours.+To+make+reservation%2C+text+RESERVE+DATE%3A%7Bmm-dd', 'outgoing', '265463', 0, '2015-08-26 15:29:14', '2015-08-26 15:29:14', ''),
(59, 'TivVuZVgg2', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '265463', 0, '2015-08-26 15:30:18', '2015-08-26 15:30:18', ''),
(60, '1LyVRFdD2F', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '265463', 0, '2015-08-26 15:30:19', '2015-08-26 15:30:19', ''),
(61, 'ABGZvQlnkM', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '265463', 0, '2015-08-26 15:30:34', '2015-08-26 15:30:34', ''),
(62, '90SqGDwc1s', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '265463', 0, '2015-08-26 15:30:34', '2015-08-26 15:30:34', ''),
(63, '67MbWC24TG', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '265463', 0, '2015-08-26 15:30:41', '2015-08-26 15:30:41', ''),
(64, 'oOjMXC1Rie', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '265463', 0, '2015-08-26 15:30:41', '2015-08-26 15:30:41', ''),
(65, 'oqwQfpdcWj', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '265463', 0, '2015-08-26 15:31:37', '2015-08-26 15:31:37', ''),
(66, 'uvi4UuGbBl', '+Here+are+the+availble+destinations+TUMBUKTHREE.', 'outgoing', '265463', 0, '2015-08-26 15:31:37', '2015-08-26 15:31:37', ''),
(67, 'WB8PpOB7VL', 'Thank+you+for+registering+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION+ex.+RES', 'outgoing', '12312389', 0, '2015-08-26 15:31:54', '2015-08-26 15:31:54', ''),
(68, 'CXPVgzZh8E', '+Here+are+the+availble+destinations+TUMBUKTHREE.', 'outgoing', '12312389', 0, '2015-08-26 15:31:54', '2015-08-26 15:31:54', ''),
(69, 'IhkrSUqEOD', 'This+number+is+already+registered+in+Rose+Tours.+For+reservations+text+RESERVE+DATE+TIME+DESTINATION', 'outgoing', '12312389', 0, '2015-08-26 15:32:32', '2015-08-26 15:32:32', ''),
(70, 'wvmpeISJDu', '+Here+are+the+availble+destinations+TUMBUKTHREE.', 'outgoing', '12312389', 0, '2015-08-26 15:32:32', '2015-08-26 15:32:32', ''),
(71, 'I4q2C9xWGL', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:20:57', '2015-08-26 16:20:57', ''),
(72, 'uA8JgBHqi2', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:21:10', '2015-08-26 16:21:10', ''),
(73, 'BEAwxMB8JE', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:21:46', '2015-08-26 16:21:46', ''),
(74, 'itUB9Diu7c', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:22:20', '2015-08-26 16:22:20', ''),
(75, 'BEGh2UOslD', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:22:21', '2015-08-26 16:22:21', ''),
(76, 'XOIF7YTlJ0', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:22:23', '2015-08-26 16:22:23', ''),
(77, '4wNwZwKBRc', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:22:24', '2015-08-26 16:22:24', ''),
(78, 'AQoTKkPseG', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:22:36', '2015-08-26 16:22:36', ''),
(79, 'RevYI4wN7i', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:23:15', '2015-08-26 16:23:15', ''),
(80, 'KfiK9WWNeW', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:23:17', '2015-08-26 16:23:17', ''),
(81, 'ihUQ4wHLto', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:23:29', '2015-08-26 16:23:29', ''),
(82, 'xhfdUuAPMZ', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:23:50', '2015-08-26 16:23:50', ''),
(83, 'eBCSnmoJLs', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:24:00', '2015-08-26 16:24:00', ''),
(84, 'zEBJ7eCwwz', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:24:09', '2015-08-26 16:24:09', ''),
(85, '0GIgvycolY', 'We+have+no+vacant+seat+for+this+schedule+2015-08-26+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '12312389', 0, '2015-08-26 16:24:18', '2015-08-26 16:24:18', ''),
(86, 'f8AMFSKEZj', 'We+have+a+vacant+seat+for+this+schedule+2015-08-26+10%3A30PM+ILOILO.+Here+is+your+reservation+code.+', 'outgoing', '12312389', 0, '2015-08-26 16:25:27', '2015-08-26 16:25:27', ''),
(87, 'mgF1xVM7iX', 'DESTINATION%3AILOILO+is+invalid.+Here+are+the+availble+destinations+TUMBUKTHREE%2CILOILO.', 'outgoing', '12312389', 0, '2015-08-26 16:32:52', '2015-08-26 16:32:52', ''),
(88, 'llwTtuNJNz', 'We+have+no+vacant+seat+for+this+schedule+2015-08-2015+1%3A30PM+ILOILO.+Here+are+the+available+schedu', 'outgoing', '12312389', 0, '2015-08-26 16:33:01', '2015-08-26 16:33:01', ''),
(89, 'dZ2UdLpmMC', 'We+have+no+vacant+seat+for+this+schedule+2015-08-2015+1%3A00PM+ILOILO.+Here+are+the+available+schedu', 'outgoing', '12312389', 0, '2015-08-26 16:33:12', '2015-08-26 16:33:12', ''),
(90, 'Xw4m0ecIvr', 'We+have+a+vacant+seat+for+this+schedule+2015-08-2015+1%3A00AM+ILOILO.+Here+is+your+reservation+code.', 'outgoing', '12312389', 0, '2015-08-26 16:33:19', '2015-08-26 16:33:19', ''),
(91, 'bJVef9btA9', 'We+have+a+vacant+seat+for+this+schedule+2015-08-2015+10%3A30PM+ILOILO.+Here+is+your+reservation+code', 'outgoing', '12312389', 0, '2015-08-26 16:34:47', '2015-08-26 16:34:47', ''),
(92, 'tTNgXMprjx', 'We+have+a+vacant+seat+for+this+schedule+2015-08-12+10%3A30PM+ILOILO.+Here+is+your+reservation+code.+', 'outgoing', '12312389', 0, '2015-08-26 16:35:41', '2015-08-26 16:35:41', ''),
(93, 'itjjV8BBaQ', 'You+have+reserved+your+trip+with+this+us+on+2015-08-12+01%3A00AM+to+destination+Iloilo', 'outgoing', '12312389', 0, '2015-08-26 16:39:23', '2015-08-26 16:39:23', ''),
(94, 'WQ3IO5XjRp', 'You+have+reserved+your+trip+with+this+us+on+2015-08-12+22%3A30PM+to+destination+Iloilo', 'outgoing', '12312389', 0, '2015-08-26 16:39:47', '2015-08-26 16:39:47', ''),
(95, 'bTHv3XqDpr', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+22%3A30PM+to+destination+Iloilo', 'outgoing', '12312389', 0, '2015-08-26 16:40:13', '2015-08-26 16:40:13', ''),
(96, 'CuAQois6jL', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+22%3A30PM+to+destination+Iloilo', 'outgoing', '12312389', 0, '2015-08-26 16:40:42', '2015-08-26 16:40:42', ''),
(97, 'lIv86Dx63l', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+destination+Iloilo', 'outgoing', '12312389', 0, '2015-08-26 16:40:49', '2015-08-26 16:40:49', ''),
(98, 'vU4N1fusal', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.', 'outgoing', '12312389', 0, '2015-08-26 16:41:20', '2015-08-26 16:41:20', ''),
(99, '8jxWnOee2W', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '12312389', 0, '2015-08-26 16:41:26', '2015-08-26 16:41:26', ''),
(100, 'PWtarhwrDZ', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '12312389', 0, '2015-08-26 16:42:31', '2015-08-26 16:42:31', ''),
(101, '3UUcRLa8kE', 'Invalid+keywords.', 'outgoing', '12312389', 0, '2015-08-26 16:43:38', '2015-08-26 16:43:38', ''),
(102, 'MjHpHlFnpk', 'You+are+not+registered+in+Rose+Tours.+To+register%2C+text+REGISTER+NAME%3A%7BYOUR+NAME+HERE%7D.+For+', 'outgoing', '231321', 0, '2015-08-26 16:44:15', '2015-08-26 16:44:15', ''),
(103, 'a7PImaIwfW', 'You+are+not+registered+in+Rose+Tours.+To+register%2C+text+REGISTER+NAME%3A%7BYOUR+NAME+HERE%7D.+For+', 'outgoing', '231321', 0, '2015-08-27 04:38:16', '2015-08-27 04:38:16', ''),
(104, 'obcg4DyUX1', 'You+are+not+registered+in+Rose+Tours.+To+register%2C+text+REGISTER+NAME%3A%7BYOUR+NAME+HERE%7D.+For+', 'outgoing', '0', 0, '2015-08-27 04:38:46', '2015-08-27 04:38:46', ''),
(105, '1ZLF5yHrIe', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:39:16', '2015-08-27 04:39:16', ''),
(106, 'qD6um7u2XD', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:39:32', '2015-08-27 04:39:32', ''),
(107, 'eRs0hvyB67', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:39:37', '2015-08-27 04:39:37', ''),
(108, 'tfFfWhs1ay', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:39:38', '2015-08-27 04:39:38', ''),
(109, 'r1Sp4kiHky', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:39:51', '2015-08-27 04:39:51', ''),
(110, 'IiPZBhUea0', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-12+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:40:04', '2015-08-27 04:40:04', ''),
(111, 'vFn6nc1K2n', '2015-08-1+is+invalid.+Please+try+again.', 'outgoing', '265463', 0, '2015-08-27 04:46:12', '2015-08-27 04:46:12', ''),
(112, 'YzTxePJtpm', 'We+have+no+vacant+seat+for+this+schedule+2015-08-28+1%3A30PM+ILOILO.+Here+are+the+available+schedule', 'outgoing', '265463', 0, '2015-08-27 04:46:29', '2015-08-27 04:46:29', ''),
(113, 'T6cd9veKcy', 'We+have+a+vacant+seat+for+this+schedule+2015-08-28+10%3A30PM+ILOILO.+Here+is+your+reservation+code.+', 'outgoing', '265463', 0, '2015-08-27 04:46:36', '2015-08-27 04:46:36', ''),
(114, 'Ea7PImaIwf', 'Invalid+reservation+code.+Please+try+again.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:47:34', '2015-08-27 04:47:34', ''),
(115, 'WAphxEerxE', 'You+have+reserved+your+trip+with+Rose+Tours+at+2015-08-28+10%3A30PM+to+Iloilo.+Thank+you%21', 'outgoing', '265463', 0, '2015-08-27 04:48:26', '2015-08-27 04:48:26', '');

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `id` int(11) NOT NULL,
  `schedule_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `code` varchar(30) NOT NULL,
  `confirmed` tinyint(4) NOT NULL,
  `schedule_date` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`id`, `schedule_id`, `customer_id`, `date`, `code`, `confirmed`, `schedule_date`) VALUES
(1, 1, 5, '2015-08-26 16:35:41', '8yF1wk', 0, '2015-08-12'),
(2, 1, 2, '2015-08-27 04:46:36', 'bVI0bt', 0, '2015-08-28');

-- --------------------------------------------------------

--
-- Table structure for table `rewards`
--

CREATE TABLE IF NOT EXISTS `rewards` (
  `id` int(10) NOT NULL,
  `driver_id` int(10) NOT NULL,
  `name` varchar(60) NOT NULL,
  `amount` float NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rewards`
--

INSERT INTO `rewards` (`id`, `driver_id`, `name`, `amount`) VALUES
(4, 2, 'thank you', 12399),
(5, 2, 'again', 341),
(6, 3, 'Birthday', 1000),
(7, 2, 'abcd', 1000),
(8, 2, 'Money', 10000),
(9, 2, 'TV', 52000);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(11) NOT NULL,
  `van_id` int(11) NOT NULL,
  `driver_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `time` time NOT NULL,
  `destination_id` int(11) NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `van_id`, `driver_id`, `user_id`, `time`, `destination_id`, `date`) VALUES
(1, 2, 2, 5, '22:30:00', 2, '2016-03-17 00:00:00'),
(2, 2, 3, 5, '22:30:00', 3, '0000-00-00 00:00:00'),
(3, 2, 2, 5, '01:00:00', 3, '0000-00-00 00:00:00'),
(4, 2, 3, 5, '00:00:00', 3, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `user_type_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `contact_no`, `user_type_id`, `status`, `username`, `password`) VALUES
(5, 'Mr', 'Manger', '09124568796', 1, 1, 'manager', 'f2053ad6a18ebbff9cd4494f1289c16d8ab250e7'),
(6, 'Ms', 'Staff', '0915467696', 3, 1, 'staff', 'f2053ad6a18ebbff9cd4494f1289c16d8ab250e7');

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE IF NOT EXISTS `user_types` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`id`, `type`) VALUES
(1, 'Manager'),
(3, 'Staff');

-- --------------------------------------------------------

--
-- Table structure for table `vans`
--

CREATE TABLE IF NOT EXISTS `vans` (
  `id` int(10) NOT NULL,
  `plate_no` varchar(10) NOT NULL,
  `status` text NOT NULL,
  `capacity` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `vans`
--

INSERT INTO `vans` (`id`, `plate_no`, `status`, `capacity`) VALUES
(2, 'XXY-245', 'new', 10),
(3, 'asdakdjk`', 'avail', 0),
(4, 'idaiosud', 'Available', 20);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedbacks`
--
ALTER TABLE `feedbacks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `message_id` (`message_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rewards`
--
ALTER TABLE `rewards`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vans`
--
ALTER TABLE `vans`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(30) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `feedbacks`
--
ALTER TABLE `feedbacks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=116;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `rewards`
--
ALTER TABLE `rewards`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `vans`
--
ALTER TABLE `vans`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
